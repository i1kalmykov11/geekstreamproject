# GeekStreamProject

Ссылка на возможный дизайн: https://www.figma.com/file/VY2E0zr5W77kBcK1crzP0d/GeekStream?node-id=0%3A1
Описание проекта и требования: https://docs.google.com/document/d/1mJR-ZjpwOntnn_K6wRJJ2lPDwMDUHOqFOg5Bw1VNWr8/edit?usp=sharing

<img src="/images/SertificateIvanKalmykov.png" width="550px" height="425px">
<img src="/images/signInGeekStream.png" width="720px" height="405px">
<img src="/images/registrationGeekStream.png" width="720px" height="405px">
<img src="/images/ConfirmationGeekStream.png" width="720px" height="405px">
<img src="/images/PasswordRecoveringGeekStream.png" width="720px" height="405px">
<img src="/images/CategoriesGeekStream.png" width="720px" height="405px">
<img src="/images/UserProfileGeekStream.png" width="720px" height="405px">
<img src="/images/UsersGeekStream.png" width="720px" height="405px">
<img src="/images/ArticleCreationGeekStream.png" width="720px" height="405px">
<img src="/images/ArticleGeekStreamOne.png" width="720px" height="405px">
<img src="/images/ArticleGeekStreamTwo.png" width="720px" height="405px">
<img src="/images/CategoryArticlesGeekStream.png" width="720px" height="405px">
<img src="/images/DialogsGeekStream.png" width="720px" height="405px">
<img src="/images/FlowGeekStream.png" width="720px" height="405px">

