using GeekStreamProject.Core.Shared.Entities;
using System;
using System.Collections.Generic;
using Xunit;

namespace GeekStream.Tests
{
    /*
    public class ArticleTests
    {
        [Fact]
        public void ConstructorShouldFailWhenTitleIsNull()
        {
            int id = 1;
            Client client = new Client("nickname", "login", "paswword", "name", "surname", 38, new List<Article>());
            string title = null;
            string content = "Cringe content";
            Rating rating = Rating.None;
            Category category = Category.Science;
            List<Comment> commentaries = new List<Comment>();
            Review review = new Review(1,client, "Content", Rating.None);

            Assert.Throws<ArgumentException>(() =>
            {
                var article = new Article(id,client, content, rating, category, title, commentaries, review);
            });
        }

        [Fact]
        public void ConstructorShouldFailWhenContentIsNull()
        {
            int id = 1;
            Client client = new Client("nickname", "login", "paswword", "name", "surname", 38, new List<Article>());
            string title = "CringeTitle";
            string content = null;
            Rating rating = new Rating();
            Category category = Category.Science;
            List<Comment> commentaries = new List<Comment>();
            Review review = new Review(1, client, "Content", new Rating());

            Assert.Throws<ArgumentException>(() =>
            {
                var article = new Article(id, client, content, rating, category, title, commentaries, review);
            });
        }

        [Fact]
        public void ConstructorShouldFailWhenCategoryIsUncorrect()
        {
            int id = 1;
            Client client = new Client("nickname", "login", "paswword", "name", "surname", 38, new List<Article>());
            string title = "CringeTitle";
            string content = "CringeContent";
            Rating rating = new Rating();
            Category category = Category.None;
            List<Comment> commentaries = new List<Comment>();
            Review review = new Review(1, client, "Content", new Rating());

            Assert.Throws<ArgumentException>(() =>
            {
                var article = new Article(id, client, content, rating, category, title, commentaries, review);
            });
        }

        [Fact]
        public void ConstructorShouldFailWhenIdIsUncorrect()
        {
            int id = -1;
            Client client = new Client("nickname", "login", "paswword", "name", "surname", 38, new List<Article>());
            string title = "CringeTitle";
            string content = "CringeContent";
            Rating rating = new Rating();
            Category category = Category.Games;
            List<Comment> commentaries = new List<Comment>();
            Review review = new Review(1, client, "Content", new Rating());

            Assert.Throws<ArgumentException>(() =>
            {
                var article = new Article(id, client, content, rating, category, title, commentaries, review);
            });
        }

        [Fact]
        public void ConstructorShouldFailWhenCommentraiesIsUncorrect()
        {
            int id = 1;
            Client client = new Client("nickname", "login", "paswword", "name", "surname", 38, new List<Article>());
            string title = "CringeTitle";
            string content = "CringeContent";
            Rating rating = Rating.None;
            Category category = Category.Games;
            List<Comment> commentaries = null ;
            Review review = new Review(1, client, "Content", new Rating());

            Assert.Throws<ArgumentException>(() =>
            {
                var article = new Article(id, client, content, rating, category, title, commentaries, review);
            });
        }
    
    }*/
}
