﻿using GeekStream.Core.Abstractions;
using GeekStream.Core.Services;
using GeekStream.Core.ViewModels;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.AspNetCore.Http;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace GeekStream.Tests
{
    public class UserServiceTest
    {
        private readonly UserService _service;
        private readonly Mock<IUserRepository> _userRepMock = new Mock<IUserRepository>();
        private readonly Mock<IHttpContextAccessor> _httpAccesMock = new Mock<IHttpContextAccessor>();
        private readonly Mock<IImageRepository<Avatar>> _imageMock = new Mock<IImageRepository<Avatar>>();
        public UserServiceTest() 
        {
            _service = new UserService(_userRepMock.Object, _httpAccesMock.Object, _imageMock.Object);
        }

        [Fact]
        public void UserServiceGetUserByIdIsNotNull()
        {
            var id = Guid.NewGuid().ToString();
            var FN = "N";
            var LN = "LN";
            var UN = "UN";
            var expected = new ApplicationUser
            {
                Id = id,
                FirstName = FN,
                LastName = LN,
                UserName = UN,
                Comments = new List<Comment>(),
                Articles = new List<Article>(),
                Avatar = new Avatar()
            };

            _userRepMock.Setup(user => user.GetUserById(id)).Returns(expected);

            var result = _service.GetUserById(id);

            Assert.Equal(expected.Id, result.Id);
        }

        [Fact]
        public void UserServiceGetUserByIdIsNull()
        {
            _userRepMock.Setup(user => user.GetUserById(It.IsAny<Guid>().ToString())).Returns(() => null);

            var result = _service.GetUserById(Guid.NewGuid().ToString());

            Assert.Null(result);
        }


        //[Fact]
        //public void IUserServiceGetUserByIdIsNotNull()
        //{
        //    var id = Guid.NewGuid().ToString();
        //    var expected = new UserViewModel()
        //    {
        //        Id = id,
        //        FirstName = "N",
        //        LastName = "LN",
        //        UserName = "UN",
        //        Email = "Email@mail.ru",
        //        CreatedDate = DateTime.UtcNow,
        //        Confirmed = true,
        //    };

        //    Mock<IUserService> mock = new Mock<IUserService>();

        //    mock.Setup(user => user.GetUserById(id)).Returns(expected);

        //    var serv = mock.Object;

        //    var result = serv.GetUserById(id);
        //    Assert.Equal(expected, result);
        //}

        //[Fact]
        //public void UserServiceGetUserByIdIsNull()
        //{
        //    var id = Guid.NewGuid().ToString();

        //    var expected = new UserViewModel()
        //    {
        //        Id = id,
        //        FirstName = "N",
        //        LastName = "LN",
        //        UserName = "UN",
        //        Email = "Email@mail.ru",
        //        CreatedDate = DateTime.UtcNow,
        //        Confirmed = true,
        //    };

        //    Mock<IUserRepository> UserRepMock = new Mock<IUserRepository>();
        //    Mock<IHttpContextAccessor> HttpAccesMock = new Mock<IHttpContextAccessor>();
        //    Mock<IImageRepository<Avatar>> ImageMock = new Mock<IImageRepository<Avatar>>();

        //    var service = new UserService(UserRepMock.Object, HttpAccesMock.Object, ImageMock.Object);

        //    var result = service.GetUserById(null);

        //    Assert.Null(result);
        //}

    }
}