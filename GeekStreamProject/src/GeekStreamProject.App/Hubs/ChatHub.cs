﻿
using GeekStream.Core.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GeekStreamProject.App.Hubs
{
    public class ChatHub : Hub
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private IMessageService _chatService;
        public ChatHub(IMessageService chatService, IHttpContextAccessor httpContextAccessor)
        {
            _chatService = chatService;
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task SendChatMessage(string message, string chatId)
        {
            var user = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Name);
            _chatService.CreateChatMessage(new GeekStream.Core.ViewModels.MessageViewModel 
            {
                Content = message,
                ChatId =  Convert.ToInt32(chatId),
            });
            await Clients.All.SendAsync("ReceiveMessage", user, message, DateTime.UtcNow.ToString());
        }
    }
}
