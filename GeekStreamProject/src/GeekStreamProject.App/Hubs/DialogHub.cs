﻿using GeekStream.Core.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GeekStreamProject.App.Hubs
{
    public class DialogHub : Hub
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMessageService _messageService;
        public DialogHub(IMessageService chatService, IHttpContextAccessor httpContextAccessor)
        {
            _messageService = chatService;
            _httpContextAccessor = httpContextAccessor;
        }
        
        public async Task SendDialogMessage(string receiverName, string message)
        {
            var user = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Name);
            _messageService.CreateDialogMessage(new GeekStream.Core.ViewModels.DialogMessageViewModel
            {
                ReceiverName = receiverName,
                Content = message,
            });
            await Clients.All.SendAsync("ReceiveMessage", user, message, DateTime.UtcNow.ToString());
        }
    }
}
