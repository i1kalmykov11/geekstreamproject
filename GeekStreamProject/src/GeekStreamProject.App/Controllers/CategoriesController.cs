﻿using GeekStream.Core.Abstractions;
using GeekStream.Core.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeekStreamProject.App.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly ICategoryService _categoryService;
        private readonly IUserService _userService;

        public CategoriesController(ICategoryService categoryService, IUserService userService)
        {
            _categoryService = categoryService;
            _userService = userService;
        }

        [HttpGet]
        public IActionResult Categories()
        {
            var categories = _categoryService.GetCategories();

            if (categories == null)
            {
                return NotFound();
            }
            ViewData["Title"] = "Категории";
            ViewBag.Users = _userService.GetUsers();
            return View(categories);
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewData["Title"] = "Создание категории";
            ViewBag.Users = _userService.GetUsers();
            return View();
        }
        [HttpPost]
        public IActionResult Create(CategoryViewModel model)
        {
            _categoryService.Create(model);
            return RedirectToAction("Articles", "Articles");
        }

        
    }
        
}
