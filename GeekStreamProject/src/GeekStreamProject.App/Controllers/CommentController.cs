﻿using GeekStream.Core.Abstractions;
using GeekStream.Core.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace GeekStreamProject.App.Controllers
{
    public class CommentController : Controller
    {
        private readonly ICommentService _commentService;
        
        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        [HttpPost]
        public IActionResult Create(CreateCommentViewModel commentViewModel)
        {
            _commentService.Create(commentViewModel);

            return RedirectToAction("Article", "Articles", new { Id = commentViewModel.ArticleId});
        }

        [Route("ChangeHonor/{authorId}")]
        [HttpGet]
        public IActionResult ChangeHonor(int id, int delta, string authorId)
        {
            _commentService.ChangeHonor(id, delta);

            return RedirectToAction("UserInfo", "Users", new { userId = authorId });
        }

        [Route("ChangeHonor/{articleId:int}")]
        [HttpGet]
        public IActionResult ChangeHonor(int id, int delta, int articleId)
        {
            _commentService.ChangeHonor(id, delta);

            return RedirectToAction("Article", "Articles", new { Id = articleId });
        }
    }
}
