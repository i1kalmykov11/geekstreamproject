﻿using GeekStream.Core.Abstractions;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace GeekStreamProject.App.Controllers
{
    public class EmailController : Controller
    {
        private readonly IEmailService _emailService;
        private readonly UserManager<ApplicationUser> _userManager;

        public EmailController(IEmailService emailService, UserManager<ApplicationUser> userManager)
        {
            _emailService = emailService;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult ConfirmEmail(string userId, string token)
        {
            var user = _userManager.FindByIdAsync(userId).GetAwaiter().GetResult();
            if (user.EmailConfirmed)
            {
                return View("AlreadyConfirmed");
            }

            bool result = _emailService.ConfirmUser(token, userId);

            if (!result)
            {
                return NotFound();
            }

            return View("EmailSuccessfulyConfirmed");
        }

        [HttpGet]
        public IActionResult UnconfirmedEmail()
        {
            return View();
        }
    }
}
