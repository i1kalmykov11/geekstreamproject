﻿using GeekStream.Core.Abstractions;
using GeekStream.Core.ViewModels;
using Microsoft.AspNetCore.Mvc;


namespace GeekStreamProject.App.Controllers
{
    public class MessageController : Controller
    {
        private readonly IMessageService _chatService;
        private readonly IUserService _userService;

        public MessageController(IMessageService chatService, IUserService userService)
        {
            _chatService = chatService;
            _userService = userService;
        }

        [HttpGet]
        public IActionResult CreateChat()
        {
            return View();
        }
        [HttpPost]
        public IActionResult CreateChat(MessangerViewModel chat)
        {
            _chatService.CreateChat(chat);
            return RedirectToAction("Chats");
        }

        [HttpGet]
        public IActionResult Chats()
        {
            ViewBag.Users = _userService.GetUsers();
            ViewBag.Chats = _chatService.GetChats();
            return View("Chats");
        }

        [HttpGet]
        public IActionResult Chat(int id)
        {
            var chat = _chatService.GetChat(id);

            if (chat == null)
            {
                return NotFound();
            }

            ViewBag.Users = _userService.GetUsers();
            ViewBag.Chats = _chatService.GetChats();
            return View("Chats", chat);
        }

        [HttpGet]
        public IActionResult Dialog(string userName)
        {
            var dialog = _chatService.GetLatestDialogMessages(userName);
            ViewData["DialogReciever"] = userName;

            if(dialog == null)
            {
                return NotFound();
            }
            ViewBag.Users = _userService.GetUsers();
            return View("Dialog", dialog);
        }
    }
}
