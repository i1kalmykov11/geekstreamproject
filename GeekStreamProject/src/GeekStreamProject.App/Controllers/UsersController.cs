﻿using Microsoft.AspNetCore.Mvc;
using GeekStream.Core.ViewModels;
using GeekStream.Core.Abstractions;
using Microsoft.AspNetCore.Identity;
using GeekStreamProject.Core.Shared.Entities;
using System.Threading.Tasks;

namespace GeekStreamProject.App.Controllers
{
    public class UsersController : Controller
    {
        private const string defaultAvatrId = "5FCE67C0-5434-4970-A9F0-CD94F58C70DA";
        private readonly IUserService _userService;
        private readonly ISubService _subService;
        private readonly IEmailService _emailService;
        private readonly ICategoryService _categoryService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        public UsersController(IUserService userService,UserManager<ApplicationUser> userManager
            ,SignInManager<ApplicationUser> signInManager, ISubService subService
            , IEmailService emailService, ICategoryService categoryService)
        {
            _userService = userService;
            _userManager = userManager;
            _signInManager = signInManager;
            _subService = subService;
            _emailService = emailService;
            _categoryService = categoryService;
        }
        
        [HttpGet]
        public IActionResult Registration()
        {
            ViewData["Title"] = "Создание аккаунта";
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Registration(RegistrationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _userService.Register(model);

            if (result.Succeeded)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                var callbackUrl = Url.Action("ConfirmEmail", "Email", new { userId = user.Id, token = token }, protocol: HttpContext.Request.Scheme);

                _emailService.SendEmail(user.Email, "Подтвердите аккаунт", $"Пожалуйста, перейдите по ссылке для подтверждения аккаунта: '{callbackUrl}'");

                var signInResult = await _signInManager.PasswordSignInAsync(user, model.Password, false, false);

                if (signInResult.IsNotAllowed)
                {
                    return Unauthorized();
                }

                return RedirectToAction("UnconfirmedEmail", "Email");
            }

            return BadRequest(result.Errors);           
        }

        [HttpGet]
        public IActionResult Login()
        {
            ViewData["Title"] = "Войти на GeekStream";
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (User?.Identity?.IsAuthenticated == true)
            {
                await _signInManager.SignOutAsync();
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user != null)
            {
                var signInResult = await _signInManager.PasswordSignInAsync(user, model.Password, false, false);
                if (signInResult.IsNotAllowed)
                {
                    return Unauthorized();
                }
                if (signInResult.Succeeded)
                {
                    return RedirectToAction("Articles","Articles");
                }
            }
            else
            {
                ModelState.AddModelError("", "Неверный логин или пароль");
            }

            return StatusCode(500);
        }

        [HttpGet]
        public async Task<IActionResult> UserSignOut()
        {
            if (User?.Identity?.IsAuthenticated == true)
            {
                await _signInManager.SignOutAsync();
                return RedirectToAction("Articles", "Articles"); ;
            }

            return RedirectToAction("Articles", "Articles");
        }

        [HttpGet]
        public IActionResult ForgetPassword()
        {
            ViewData["Title"] = "Забыли пароль? не беда";
            return View();
        }

        [HttpPost]
        public IActionResult ForgetPassword(ForgetPasswordViewModel model)
        {
            var user = _userManager.FindByEmailAsync(model.Email).GetAwaiter().GetResult();
            if (user != null && _userManager.IsEmailConfirmedAsync(user).GetAwaiter().GetResult())
            {
                if (ModelState.IsValid)
                {
                    var token = _userManager.GeneratePasswordResetTokenAsync(user).GetAwaiter().GetResult();
                    var callbackUrl = Url.Action("ChangePassword", "Users", new { userId = user.Id, token = token }, protocol: HttpContext.Request.Scheme);
                    _emailService.SendEmail(user.Email, "Сброс пароля", $"Пожалуйста, перейдите по ссылке чтобы сбросить пароль: '{callbackUrl}'");

                    ViewData["Title"] = "Письмо подтверждения сброса пароля отправлено";
                    return View("ResetPassword");
                }
            }
            
            return View(model.Email);
        }


        [HttpGet]
        public IActionResult ChangePassword(string userId, string token)
        {
            var user = _userManager.FindByIdAsync(userId).GetAwaiter().GetResult();
            if (user == null)
            {
                return NotFound();
            }

            var newPasswordViewModel = new NewPasswordViewModel()
            {
                Token = token,
                UserId = userId,
            };

            ViewData["Title"] = "Задайте новый пароль";
            return View(newPasswordViewModel);
        }

        [HttpPost]
        public IActionResult ChangePassword(NewPasswordViewModel newPassword)
        {
            if (!ModelState.IsValid)
            {
                return NotFound();
            }

            var user = _userManager.FindByIdAsync(newPassword.UserId).GetAwaiter().GetResult();

            if (user == null)
            {
                return NotFound();
            }

            var changed = _userManager.ResetPasswordAsync(user, newPassword.Token, newPassword.Password).GetAwaiter().GetResult();

            if (changed.Succeeded)
            {
                ViewData["Title"] = "Изменение пароля";
                return View("PasswordSuccessfullyChanged");
            }
            else
            {
                return View(newPassword);
            }
        }

        public IActionResult MyInfo()
        {
            var user = _userService.GetUserById();

            if (user == null)
            {
                return NotFound();
            }

            ViewBag.Categories = _categoryService.GetCategories();
            ViewBag.Subscriptions = _subService.GetSubscriptions();
            ViewBag.Users = _userService.GetUsers();
            ViewData["Title"] = "Мой профиль";

            return View("UserInfo",user);
        }

        [HttpGet("UserInfo/{userId?}")]
        public IActionResult UserInfo(string userId)
        {
            var user = _userService.GetUserById(userId);
            

            if (user == null)
            {
                return NotFound();
            }

            ViewData["isSubscribed"] = _subService.IsSubscribed(user.Id);
            ViewBag.Categories = _categoryService.GetCategories();
            ViewBag.Subscriptions = _subService.GetSubscriptions();
            ViewBag.Users = _userService.GetUsers();
            ViewData["Title"] = user.UserName;
            return View(user);
        }

        [HttpGet]
        public IActionResult Authors()
        {
            var authors = _userService.GetAuthors();
            if (authors == null)
            {
                return NotFound();
            }

            ViewBag.Categories = _categoryService.GetCategories();
            ViewBag.Subscriptions = _subService.GetSubscriptions();
            ViewData["Title"] = "Авторы";
            return View("Users", authors);
        }

        [HttpGet]
        public IActionResult Users()
        {
            var users = _userService.GetUsers();
            if (users == null)
            {
                return NotFound();
            }

            ViewBag.Categories = _categoryService.GetCategories();
            ViewBag.Subscriptions = _subService.GetSubscriptions();
            ViewData["Title"] = "Пользователи";
            return View("Users", users);
        }

        [HttpGet]
        public IActionResult Subscribe(string userId)
        {
            var user = _userService.GetUserById(userId);

            if(user == null)
            {
                return NotFound();
            }

            _subService.Subscribe(user);

            return RedirectToAction("UserInfo", new { userId = user.Id });
        }

        [HttpGet]
        public IActionResult Unsubscribe(string userId)
        {
            var user = _userService.GetUserById(userId);

            if (user == null)
            {
                return NotFound();
            }

            _subService.Unsubscribe(user);

            return RedirectToAction("UserInfo", new { userId = user.Id });
        }

        [HttpGet("Subscribers/{userId?}")]
        public IActionResult Subscribers(string userId)
        {
            var user = _userService.GetUserById(userId);

            if (user == null)
            {
                return NotFound();
            }

            var subs = _subService.GetSubscribers(user);
            
            if (subs == null)
            {
                return NotFound("Subs");
            }

            ViewBag.Categories = _categoryService.GetCategories();
            ViewBag.Subscriptions = _subService.GetSubscriptions();
            ViewData["Title"] = "Подписчики";
            return View("Users", subs);
        }

        [HttpGet("Subscriptions/{userId?}")]
        public IActionResult Subscriptions(string userId)
        {
            var user = _userService.GetUserById(userId);

            if (user == null)
            {
                return NotFound();
            }

            var subs = _subService.GetSubscriptions(user);
            if (subs == null)
            {
                return NotFound();
            }

            ViewBag.Categories = _categoryService.GetCategories();
            ViewBag.Subscriptions = _subService.GetSubscriptions();
            ViewData["Title"] = "Подписки";
            return View("Users", subs);
        }

        [HttpGet]
        public IActionResult Edit(string userId)
        {
            var user =  _userService.GetUserById(userId);
            if (user == null)
            {
                return View();
            }
            ViewData["Title"] = "Редактирование профиля";
            return View(user);
        }

        [HttpPost]
        public IActionResult Edit(UserViewModel user)
        {
            _userService.Edit(user);
            return RedirectToAction("UserInfo", new { userId = user.Id});
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmEmail(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);

            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);

            var callbackUrl = Url.Action("ConfirmEmail", "Email", new { userId = user.Id, token = token }, protocol: HttpContext.Request.Scheme);

            _emailService.SendEmail(user.Email, "Подтвердите аккаунт", $"Пожалуйста, перейдите по ссылке для подтверждения аккаунта: {callbackUrl}");

            return RedirectToAction("UnconfirmedEmail", "Email");
        }
    }
}
