﻿using Microsoft.AspNetCore.Mvc;
using GeekStreamProject.Core.Shared.Entities;
using GeekStream.Core.Abstractions;
using GeekStream.Core.ViewModels;
using Microsoft.AspNetCore.Identity;
using GeekStreamProject.Core.Shared;
using GeekStream.Core.Services;
using GeekStream.Core.Additional;

namespace GeekStreamProject.App.Controllers
{
    public class ArticlesController : Controller
    {
        private readonly IArticleService _articleService;
        private readonly ICategoryService _categoryService;
        private readonly IUserService _userService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ISubService _subService;
        public ArticlesController(IArticleService articleService
                                    , ICategoryService categoryService, UserManager<ApplicationUser> userManager
                                    , ISubService subService, IUserService userService
                                 )
        {
            _articleService = articleService;
            _categoryService = categoryService;
            _userManager = userManager;
            _subService = subService;
            _userService = userService;
        }

        // GET: Articles
        [HttpGet]
        public IActionResult Articles()
        {
            var articleViewModels = _articleService.GetPublishedArticles();

            if (articleViewModels == null)
            {
                return NotFound();
            }

            ViewBag.Categories = _categoryService.GetCategories();
            ViewBag.Subscriptions = _subService.GetSubscriptions();
            ViewBag.Users = _userService.GetUsers();
            ViewData["Title"] = "Лента";

            return View(articleViewModels);
        }

        [HttpGet]
        public IActionResult NotPublishedUserArticles()
        {
            var articleViewModels = _articleService.GetNotPublishedUserArticles();


            if (articleViewModels == null)
            {
                return NotFound();
            }

            ViewBag.Categories = _categoryService.GetCategories();
            ViewBag.Subscriptions = _subService.GetSubscriptions();
            ViewBag.Users = _userService.GetUsers();
            ViewData["Title"] = "Черновики";

            return View("Articles", articleViewModels);
        }

        [HttpGet]
        public IActionResult NotPublishedArticles()
        {
            var articleViewModels = _articleService.GetNotPublishedArticles();


            if (articleViewModels == null)
            {
                return NotFound();
            }

            ViewBag.Categories = _categoryService.GetCategories();
            ViewBag.Subscriptions = _subService.GetSubscriptions();
            ViewBag.Users = _userService.GetUsers();
            ViewData["Title"] = "Неопубликованные статьи";

            return View("Articles", articleViewModels);
        }

        [HttpGet]
        public IActionResult PublishedUserArticles(string userId)
        {
            var user = _userManager.FindByIdAsync(userId).GetAwaiter().GetResult();
            var articleViewModels = _articleService.GetLatestPublishedUserArticles(userId);


            if (articleViewModels == null)
            {
                return NotFound();
            }

            ViewBag.Categories = _categoryService.GetCategories();
            ViewBag.Subscriptions = _subService.GetSubscriptions();
            ViewBag.Users = _userService.GetUsers();
            ViewData["Title"] = "Статьи пользователя: " + user.UserName;

            return View("Articles", articleViewModels);
        }

        [HttpGet]
        public IActionResult NotApprovedArticles()
        {
            var articleViewModels = _articleService.GetNotApprovedArticles();


            if (articleViewModels == null)
            {
                return NotFound();
            }

            ViewBag.Categories = _categoryService.GetCategories();
            ViewBag.Subscriptions = _subService.GetSubscriptions();
            ViewBag.Users = _userService.GetUsers();
            ViewData["Title"] = "Неподтвержденные статьи";

            return View("Articles", articleViewModels);
        }

        [HttpGet]
        public IActionResult ApprovedArticles()
        {
            var articleViewModels = _articleService.GetApprovedArticles();


            if (articleViewModels == null)
            {
                return NotFound();
            }

            ViewBag.Categories = _categoryService.GetCategories();
            ViewBag.Subscriptions = _subService.GetSubscriptions();
            ViewBag.Users = _userService.GetUsers();
            ViewData["Title"] = "Подтвержденные статьи";

            return View("Articles", articleViewModels);
        }

        [HttpGet] // GET : Article/Articles/categoryId
        public IActionResult ArticlesByCategory(int id)
        {
            var articleWithCategory = _articleService.GetArticlesByCategory(id);

            if (articleWithCategory is null)
            {
                return NotFound();
            }


            ViewBag.Category = _categoryService.GetCategory(id);
            ViewBag.Categories = _categoryService.GetCategories();
            ViewBag.Subscriptions = _subService.GetSubscriptions();
            ViewBag.Users = _userService.GetUsers();
            ViewData["Title"] = "Лента по категории: " + _categoryService.GetCategory(id).Name;

            return View("Articles", articleWithCategory);
        }

        // GET: Articles/Article/articleId
        [HttpGet]
        public IActionResult Article(int id)
        {
            var articleViewModel = _articleService.GetArticle(id);

            if (articleViewModel == null)
            {
                return NotFound();
            }

            ViewBag.Categories = _categoryService.GetCategories();
            ViewBag.Subscriptions = _subService.GetSubscriptions();
            ViewBag.Users = _userService.GetUsers();

            return View(articleViewModel);
        }

        [HttpGet]
        public IActionResult SubscriptionArticles()
        {
            var subsciptions = _subService.GetSubscriptions();


            var articles = _articleService.GetSubscriptionArticles(subsciptions);

            if (articles == null)
            {
                return NotFound();
            }

            ViewBag.Categories = _categoryService.GetCategories();
            ViewBag.Subscriptions = _subService.GetSubscriptions();
            ViewBag.Users = _userService.GetUsers();
            ViewData["Title"] = "Отслеживаемое";

            return View("Articles", articles);
        }

        [HttpGet]
        public IActionResult Create()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Users");
            }
            var user = _userService.GetUserById();
            if (!user.Confirmed)
            {
                ViewData["Title"] = "Подтверждение аккаунта";
                return View("../Users/AskConfirmEmail");
            };
            ViewData["Title"] = "Создание статьи";
            ViewBag.Categories = _categoryService.GetCategories();
            return View();
        }
        [HttpPost]
        public IActionResult Create(CreateArticleViewModel articleViewModel)
        {        
            if (User.Identity.IsAuthenticated)
            {
                _articleService.Create(articleViewModel);
            }

            return RedirectToAction("Articles");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var article = _articleService.GetArticle(id);

            ViewData["Title"] = "Изменение статьи";
            ViewBag.Categories = _categoryService.GetCategories();
            return View(article);
        }
        [HttpPost]
        public IActionResult Edit(ArticleViewModel changedArticle)
        {
            if (User.Identity.IsAuthenticated)
            {
                _articleService.Edit(changedArticle);
            }

            return RedirectToAction("Article", new { Id = changedArticle.Id });
        }

        public IActionResult Delete(int id)
        {
            ViewBag.Article = _articleService.GetArticle(id);
            ViewBag.Subscriptions = _subService.GetSubscriptions();
            return View();
        }
        [HttpGet]
        public IActionResult DeleteArticle(int id)
        {
            _articleService.DeleteArticle(id);

            return RedirectToAction("Articles");
        }

        [HttpGet]
        public IActionResult ApproveArticle(int id)
        {
            _articleService.ApproveArticle(id);

            return RedirectToAction("NotApprovedArticles");
        }

        [HttpGet]
        public IActionResult PublishArticle(int id)
        {
            _articleService.PublishArticle(id);

            return RedirectToAction("NotPublishedUserArticles");
        }

        [HttpGet]
        public IActionResult DisApproveArticle(int id)
        {
            _articleService.DisApproveArticle(id);

            return RedirectToAction("NotApprovedArticles");
        }

        [HttpGet]
        public IActionResult DisPublishArticle(int id)
        {
            _articleService.DisPublishArticle(id);

            return RedirectToAction("NotPublishedUserArticles");
        }


        [HttpGet]
        public IActionResult ChangeThisHonor(int id, int delta)
        {
            _articleService.ChangeHonor(id, delta);
            var user = _userService.GetUserById();
            if (!user.Confirmed)
            {
                ViewData["Title"] = "Подтверждение аккаунта";
                return View("../Users/AskConfirmEmail");
            };
            return RedirectToAction("Article", "Articles", new { Id = id });
        }

        [HttpGet]
        public IActionResult ChangeHonor(int id, int delta)
        {
            _articleService.ChangeHonor(id, delta);
            var user = _userService.GetUserById();
            if (!user.Confirmed)
            {
                ViewData["Title"] = "Подтверждение аккаунта";
                return View("../Users/AskConfirmEmail");
            };
            return RedirectToAction("Articles", "Articles");
        }


        [HttpGet]
        public IActionResult SearchArticle(string line)
        {
            var articles = _articleService.SearchArticles(line);

            if (articles == null)
            {
                return NotFound();
            }

            ViewBag.Categories = _categoryService.GetCategories();
            ViewBag.Subscriptions = _subService.GetSubscriptions();
            ViewBag.Users = _userService.GetUsers();
            ViewData["Title"] = "Статьи по тегам";
            return View("Articles", articles);
        }
    }
}
