﻿"use strict";

(async function () {
    var connection = new signalR.HubConnectionBuilder().withUrl("/chat").build();

    //Disable send button until connection is established
    document.getElementById("sendButton").disabled = true;

    connection.on("ReceiveMessage", (user, message, date) => {
        const divExternal = document.createElement("div");
        divExternal.classList.add("container");

        const authenticatedUser = document.getElementById("authenticatedUser").value;

        const divSemiExternal = document.createElement("div");
        if (authenticatedUser == user) {
            divSemiExternal.classList.add("d-flex", "justify-content-end");
        }
        if (authenticatedUser != user) {
            divSemiExternal.classList.add("d-flex", "justify-content-start");
        }

        const divSemiInternal = document.createElement("div");

        const divInternal = document.createElement("div");
        divInternal.classList.add("card", "bg-geekStream");

        const div = document.createElement("div");
        div.classList.add("card-body");

        const h5 = document.createElement("h5");
        h5.classList.add("mb-1");
        h5.textContent = `${user}`;

        const small = document.createElement("small");
        small.classList.add("text-muted", "mb-1");
        small.textContent = `${date}`;

        const p = document.createElement("p");
        small.classList.add("mb-1");
        p.textContent = `${message}`;


        div.appendChild(h5);
        div.appendChild(p);

        divInternal.append(div);

        
        divSemiInternal.append(divInternal);
        divSemiInternal.append(small);
        divSemiExternal.append(divSemiInternal);
        divExternal.append(divSemiExternal);

        document.getElementById("messagesList").appendChild(divExternal);
         
        });

    connection.start().then(function () {
        document.getElementById("sendButton").disabled = false;
    }).catch(function (err) {
        return console.error(err.toString());
    });

    document.getElementById("sendButton").addEventListener("click", function (event) {
        const message = document.getElementById("messageInput").value;
        const chatId = document.getElementById("chatId").value;
        if (message != "") {
            connection.invoke("SendChatMessage", message, chatId).catch(function (err) {
                return console.error(err.toString());
            });
        }
        event.preventDefault();
        document.getElementById("messageInput").value = "";
    });

})();


