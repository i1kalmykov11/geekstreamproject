﻿using FluentValidation;
using GeekStream.Core.ViewModels;

namespace GeekStreamProject.App.Validation
{
    public class RegistrationViewModelValidator : AbstractValidator<RegistrationViewModel>
    {
        public RegistrationViewModelValidator()
        {
            RuleFor(user => user.UserName)
                .NotEmpty()
                .WithMessage("Введите никнейм");

            RuleFor(user => user.FirstName)
                .NotEmpty()
                .WithMessage("Введите имя");

            RuleFor(user => user.LastName)
                .NotEmpty()
                .WithMessage("Введите фамилию");

            RuleFor(user => user.Email)
                .EmailAddress()
                .NotEmpty()
                .WithMessage("Введите почтовый адрес");

            RuleFor(user => user.Password)
                .NotEmpty()
                .WithMessage("Введите пароль");

            RuleFor(user => user.ConfirmPassword)
                .Equal(user => user.Password)
                .WithMessage("Повторно укажите пароль");
            RuleFor(user => user.ConfirmPassword)
                .NotEmpty()
                .WithMessage("Повторно укажите пароль");
        }
    }
}
