﻿using FluentValidation;
using GeekStream.Core.ViewModels;

namespace GeekStreamProject.App.Validators
{
    public class UserViewModelValidator : AbstractValidator<UserViewModel>
    {
        public UserViewModelValidator()
        {
            RuleFor(user => user.UserName)
                .NotEmpty()
                .WithMessage("Введите никнейм");

            RuleFor(user => user.FirstName)
                .NotEmpty()
                .WithMessage("Введите имя");

            RuleFor(user => user.LastName)
                .NotEmpty()
                .WithMessage("Введите фамилию");

            RuleFor(user => user.Email)
                .EmailAddress()
                .NotEmpty()
                .WithMessage("Введите почтовый адрес");

        }
    }
}
