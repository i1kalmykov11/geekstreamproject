﻿using FluentValidation;
using GeekStream.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeekStreamProject.App.Validation
{
    public class CategoryViewModelValidator : AbstractValidator<CategoryViewModel>
    {
        public CategoryViewModelValidator()
        {
            RuleFor(category => category.Name)
                .NotNull()
                .WithMessage("Укажите название категории!");

            RuleFor(category => category.Name)
                .NotEmpty()
                .WithMessage("Укажите название категории!");

            RuleFor(category => category.InputImage)
                .NotNull()
                .WithMessage("Выберите подходящую картинку!");
        }
    }
}
