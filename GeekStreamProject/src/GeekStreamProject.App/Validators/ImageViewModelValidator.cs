﻿using FluentValidation;
using GeekStream.Core.ViewModels;

namespace GeekStreamProject.App.Validation
{
    public class ImageViewModelValidator : AbstractValidator<CategoryImageViewModel>
    {
        public ImageViewModelValidator()
        {
            RuleFor(image => image.File).SetValidator(new FormFileValidator());
        }
    }
}
