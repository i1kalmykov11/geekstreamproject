﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeekStreamProject.App.Validation
{
    public class FormFileValidator : AbstractValidator<IFormFile>
    {
        public FormFileValidator()
        {

            RuleFor(file => file.Length)
                .NotNull()
                .WithMessage("Выберите картинку!");

            RuleFor(file => file.Length)
                .NotEmpty()
                .WithMessage("Выберите картинку!");

            RuleFor(file => file.FileName)
                .NotNull()
                .WithMessage("Выберите картинку!");

            RuleFor(file => file.FileName)
                .NotEmpty()
                .WithMessage("Выберите картинку!");

            RuleFor(file => file.Name)
                .NotNull()
                .WithMessage("Выберите картинку!");

            RuleFor(file => file.Name)
                .NotEmpty()
                .WithMessage("Выберите картинку!");

            RuleFor(file => file.ContentType)
                .Must(x => x.Equals("image/jpeg") || x.Equals("image/jpg") || x.Equals("image/png"))
                .WithMessage("Выберите картинку!!!");

            RuleFor(file => file.ContentType)
                .NotNull()
                .WithMessage("Выберите картинку!!!");


        }
    }
}
