﻿using FluentValidation;
using GeekStream.Core.ViewModels;

namespace GeekStreamProject.App.Validation
{
    public class LoginViewModelValidator : AbstractValidator<LoginViewModel>
    {
        public LoginViewModelValidator()
        {
            RuleFor(user => user.Password)
                .NotEmpty()
                .WithMessage("Введите пароль");

            RuleFor(user => user.Email)
                .NotEmpty()
                .WithMessage("Введите почтовый адрес");

            RuleFor(user => user.Email)
                .EmailAddress()
                .WithMessage("Введите почтовый адрес");
        }
    }
}
