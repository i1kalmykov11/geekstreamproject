﻿using FluentValidation;
using GeekStream.Core.ViewModels;

namespace GeekStreamProject.App.Validation
{
    public class CreateArticleViewModelValidator : AbstractValidator<CreateArticleViewModel>
    {
        public CreateArticleViewModelValidator()
        {
            RuleFor(article => article.Title)
                .NotEmpty()
                .WithMessage("Статья должна иметь название. Назовите статью!");

            RuleFor(article => article.Title)
                .MaximumLength(300)
                .WithMessage("Название статьи не может содержать больше 300 символов!");

            RuleFor(article => article.Content)
                .NotEmpty()
                .WithMessage("Статья должна содержать текст!");

            RuleFor(article => article.Content)
                .MinimumLength(100)
                .WithMessage("Статья должна содержать более 100 символов");

            RuleFor(article => article.CategoryId)
                .NotEmpty()
                .WithMessage("Выберите категорию статьи!");

            RuleFor(article => article.CategoryId)
                .NotNull()
                .WithMessage("Выберите категорию статьи!");

            RuleFor(article => article.CategoryId)
                .NotEqual(0)
                .WithMessage("Выберите категорию статьи!");

            RuleFor(article => article.File)
                .NotNull()
                .WithMessage("Выберите картинку статьи!");

            RuleFor(article => article.File)
                .SetValidator(new FormFileValidator());
        }
    }
}
