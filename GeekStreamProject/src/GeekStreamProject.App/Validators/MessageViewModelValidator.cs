﻿using FluentValidation;
using GeekStream.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeekStreamProject.App.Validation
{
    public class MessageViewModelValidator : AbstractValidator<MessageViewModel>
    {
        public MessageViewModelValidator()
        {
            RuleFor(message => message.Content)
                .NotEmpty()
                .WithMessage("Введите комментарий");
        }
    }
}