using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Core.Shared;
using GeekStreamProject.Infrastructure.DataAccess;
using GeekStream.Core.Services;
using GeekStream.Core.Abstractions;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.App.Hubs;
using FluentValidation.AspNetCore;
using GeekStream.Core.ViewModels;
using FluentValidation;
using GeekStreamProject.App.Validation;
using Microsoft.AspNetCore.Http;
using GeekStreamProject.App.Validators;
using Microsoft.AspNetCore.Identity;

namespace GeekStreamProject
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddHttpContextAccessor()
                .AddControllersWithViews()
                .AddFluentValidation();

            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped<IArticleRepository,ArticleRepository>();
            services.AddScoped<IArticleService, ArticleService>();

            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, ApplicationUserRepository>();

            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<ICommentRepository, CommentRepository>();

            services.AddScoped<IMessageService, MessageService>();
            services.AddScoped<IMessageRepository, MessageRepository>();

            services.AddScoped<ISubService, SubService>();
            services.AddScoped<ISubcriptionRepository, SubscriptionRepository>();

            services.AddScoped<IEmailService, EmailService>();

             services.AddScoped<IImageRepository<ArticleImage>, ArticleImageRepository>();
            services.AddScoped<ICategoryImageRepository, CategoryImageRepository>();
            services.AddScoped<IImageRepository<Avatar>, UserImageRepository>();

            services.AddScoped<IFileRepository, FileRepository>();

            services.AddScoped<ITagRepository, TagRepository>();
            services.AddScoped<IArticleTagRepository, ArticleTagRepository>();
            services.AddScoped<ICurrentlyAuthenticatedUserRepository, CurrentlyAuthenticatedUserRepository>();

            services
                .AddTransient<IValidator<CreateArticleViewModel>, CreateArticleViewModelValidator>()
                .AddTransient<IValidator<ArticleViewModel>, ArticleViewModelValidator>()
                .AddTransient<IValidator<RegistrationViewModel>, RegistrationViewModelValidator>()
                .AddTransient<IValidator<IFormFile>, FormFileValidator>()
                .AddTransient<IValidator<LoginViewModel>, LoginViewModelValidator>()
                .AddTransient<IValidator<MessageViewModel>, MessageViewModelValidator>()
                .AddTransient<IValidator<CategoryImageViewModel>, ImageViewModelValidator>()
                .AddTransient<IValidator<UserViewModel>, UserViewModelValidator>()
                .AddTransient<IValidator<CategoryViewModel>, CategoryViewModelValidator>();

            services.AddSignalR(); 
            services.AddRazorPages();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Articles}/{action=Articles}");

                endpoints.MapHub<ChatHub>("/chat");
                endpoints.MapHub<DialogHub>(
                    "/Dialog"
                    );
            });
        }
    }
}
