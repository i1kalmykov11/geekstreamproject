﻿using System;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class DialogMessage : Message
    {
        public string RecieverId { get; set; }

        public virtual ApplicationUser Reciever { get; set; }

        public DateTime ReadedTime { get; set; }

    }
}
