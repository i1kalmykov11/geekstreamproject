﻿using System.Collections.Generic;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class Tag
    {
        public int Id { get; set; }

        public string Content { get; set; }

        public IEnumerable<ArticleTag> ArticleTags { get; set; }
    }
}
