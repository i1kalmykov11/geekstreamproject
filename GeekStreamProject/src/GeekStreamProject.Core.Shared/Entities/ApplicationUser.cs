﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class ApplicationUser : IdentityUser
    {

        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;

        public string AvatarId { get; set; }
        
        public virtual Avatar Avatar { get; set; }
        
        public virtual ICollection<ApplicationUserRole> ApplicationUserRoles { get; set; }
        
        public virtual IEnumerable<Article> Articles { get; set; }

        public virtual IEnumerable<Comment> Comments { get; set; }
        
        public virtual IEnumerable<SubscriptionSystem> Subscribers { get; set; }

        public virtual IEnumerable<SubscriptionSystem> Subscriptions { get; set; }

    }
}
