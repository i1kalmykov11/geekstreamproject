﻿using System;
using System.Collections.Generic;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class Article
    {
        public int Id { get; set; }
        
        public string Title { get; set; }
        
        public string Content { get; set; }

        public bool IsApproved { get; set; }

        public bool IsPublished { get; set; }

        public int Honor { get; set; }

        public DateTime? PublishedDate { get; set; }
        
        public DateTime? ApprovedDate { get; set; }

        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;

        public string AuthorId { get; set; }
        
        public virtual ApplicationUser Author { get; set; }
        
        public int CategoryId { get; set; }
        
        public virtual ArticleCategory Category { get; set; }

        public virtual IEnumerable<Comment> Comments { get; set; }

        public virtual IEnumerable<ArticleTag> ArticleTags { get; set; }
        
        public string ImageId { get; set; }

        public virtual ArticleImage Image { get; set; }
        
    }
}
