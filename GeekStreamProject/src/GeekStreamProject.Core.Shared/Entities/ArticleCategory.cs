﻿using System.Collections.Generic;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class ArticleCategory
   {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public string Discription { get; set; }

        public virtual IEnumerable<Article> Articles { get; set; }

        public string ImageId { get; set; }

        public virtual CategoryImage Image { get; set; }

        
   }
}
