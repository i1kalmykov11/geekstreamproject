﻿using System;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        
        public string Content { get; set; }
        
        public int Honor { get; set; }

        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;

        public string AuthorId { get; set; }
        
        public virtual ApplicationUser Author { get; set; }

        public int ArticleId { get; set; }

        public virtual Article Article { get; set; }
    }
}
