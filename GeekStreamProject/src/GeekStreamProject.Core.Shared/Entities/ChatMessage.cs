﻿namespace GeekStreamProject.Core.Shared.Entities
{
    public class ChatMessage : Message
    {
        public string Name { get; set; }

        public int ChatId { get; set; }

        public virtual Chat Chat { get; set; }

    }
}
