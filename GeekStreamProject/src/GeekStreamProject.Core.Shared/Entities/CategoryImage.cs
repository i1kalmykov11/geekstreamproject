﻿namespace GeekStreamProject.Core.Shared.Entities
{
    public class CategoryImage : Image
    {
        public string BackGroundPath { get; set; }

        public string BackGroundFileName { get; set; }

        public virtual ArticleCategory Category { get; set; }
    }
}
