﻿using System.Collections.Generic;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class Avatar : Image
    {
        public virtual IEnumerable<ApplicationUser> Users { get; set; }
    }
}
