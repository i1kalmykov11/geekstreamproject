﻿namespace GeekStreamProject.Core.Shared.Entities
{
    public class ArticleImage : Image
    {
        public virtual Article Article { get; set; }
    }
}
