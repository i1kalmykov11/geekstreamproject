﻿using System;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class Message
    {
        public int Id { get; set; }

        public string AuthorId { get; set; }

        public virtual ApplicationUser Author { get; set; }

        public DateTime SendedTime { get; set; } = DateTime.UtcNow;

        public string Content { get; set; }

    }
}
