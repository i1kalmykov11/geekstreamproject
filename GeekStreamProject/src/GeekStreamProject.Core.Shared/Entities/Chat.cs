﻿using System.Collections.Generic;

namespace GeekStreamProject.Core.Shared.Entities
{
    public class Chat
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<ChatMessage>? Messages { get; set; }
    }
}
