﻿
namespace GeekStreamProject.Core.Shared.Entities
{
    public class SubscriptionSystem
    {
        public int Id { get; set; }

        public string SubscriberId { get; set; }

        public ApplicationUser Subscriber { get; set; }

        public string SubscriptionId { get; set; }

        public ApplicationUser Subscription { get; set; }
    }
}
