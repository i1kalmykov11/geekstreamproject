﻿namespace GeekStreamProject.Core.Shared.Entities
{
    public class Image
    {
        public string Id { get; set; }

        public string Path { get; set; }

        public string FileName { get; set; }
    }
}
