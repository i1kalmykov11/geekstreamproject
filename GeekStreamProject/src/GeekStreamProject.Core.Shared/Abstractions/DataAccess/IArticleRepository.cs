﻿using GeekStreamProject.Core.Shared.Entities;
using System.Collections.Generic;

namespace GeekStreamProject.Core.Shared
{
    public interface IArticleRepository
    {
        public IEnumerable<Article> GetPublishedArticles();

        public IEnumerable<Article> GetNotPublishedUserArticles(string id);

        public IEnumerable<Article> GetNotPublishedArticles();

        public IEnumerable<Article> GetNotApprovedArticles();

        public IEnumerable<Article> GetApprovedArticles();

        public IEnumerable<Article> GetPublishedArticlesByCategory(int categoryId);

        public IEnumerable<Article> GetLatestPublishedUserArticles(string userId);

        public Article GetArticleInfo(int id);

        public void ApproveArticle(int id);

        public void PublishArticle(int id);

        public void DisApproveArticle(int id);

        public void DisPublishArticle(int id);

        public void Create(Article item);

        public void Edit(Article changedArticle);

        public void Delete(int id);

        public void ChangeHonor(int id, int delta);
    }
}
