﻿using GeekStreamProject.Core.Shared.Entities;
using System.Collections.Generic;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface ITagRepository
    {
        public void CreateTagsFromStringInput(string[] line);

        public void CreateTag(Tag tag);

        //public IEnumerable<Tag> GetTags(int articleId);

        public bool IsTagExist(string tagContent);

        //public void AddArticleToTag(string content, int articleId);

        public Tag GetTag(string content);

        public string TagToString(IEnumerable<string> tags);
    }
}
