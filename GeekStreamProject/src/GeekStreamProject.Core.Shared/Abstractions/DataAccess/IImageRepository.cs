﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface IImageRepository<T> : IDisposable where T : class
    {
        public void Create(T image);

        public string SaveImage(IFormFile formFile, string directoryName);

        public void Delete(string id);
    }
}
