﻿using GeekStreamProject.Core.Shared.Entities;
using System.Collections.Generic;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface ICategoryRepository
    {
        public IEnumerable<ArticleCategory> GetCategories();
        
        public ArticleCategory GetCategory(int id);

        public void Create(ArticleCategory item);
    }
}
