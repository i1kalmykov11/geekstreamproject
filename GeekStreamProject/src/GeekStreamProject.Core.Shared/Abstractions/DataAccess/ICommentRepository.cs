﻿using GeekStreamProject.Core.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface ICommentRepository
    {
        public void Create(Comment comment);
        public void ChangeHonor(int id, int delta);
    }
}
