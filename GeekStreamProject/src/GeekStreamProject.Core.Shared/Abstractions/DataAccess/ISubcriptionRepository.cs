﻿using GeekStreamProject.Core.Shared.Entities;
using System.Collections.Generic;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface ISubcriptionRepository
    {
        public IEnumerable<ApplicationUser> GetSubscribers(string userId);

        public IEnumerable<ApplicationUser> GetSubscriptions(string userId);

        public void Subscribe(string subscriberId, string subscriptionId);

        public void Unsubscribe(string subscriberId, string subscriptionId);

        public bool IsSubscribed(string subscriberId, string subscriptionId);
    }
}
