﻿using GeekStreamProject.Core.Shared.Entities;
using Microsoft.AspNetCore.Http;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface ICategoryImageRepository
    {
        public void Create(CategoryImage image);

        public string SaveImage(IFormFile avatar, IFormFile backGround, string directoryName);

        public void Delete(string id);
    }
}
