﻿using GeekStreamProject.Core.Shared.Entities;
using System.Collections.Generic;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface IMessageRepository
    {
        public void CreateChat(Chat chat);

        public void CreateChatMessage(ChatMessage chatMessage);

        public void CreateDialogMessage(DialogMessage dialogMessage);

        public Chat GetChat(int id);

        public IEnumerable<ChatMessage> GetLatestChatMessages(int id);

        public IEnumerable<DialogMessage> GetLatestDialogMessages(string user1Id, string user2Id);

        public IEnumerable<Chat> GetChats();
    }
}
