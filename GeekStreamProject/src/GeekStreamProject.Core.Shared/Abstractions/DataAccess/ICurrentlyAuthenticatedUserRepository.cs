﻿using GeekStreamProject.Core.Shared.Entities;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface ICurrentlyAuthenticatedUserRepository
    {
        public ApplicationUser GetCurrentlyAuthenticatedUser();
    }
}
