﻿using Microsoft.AspNetCore.Http;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface IFileRepository
    {
        public string SaveFile(IFormFile formFile, string directoryName);

        public void DeleteFile(string path);
    }
}
