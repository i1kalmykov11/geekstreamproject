﻿using GeekStreamProject.Core.Shared.Entities;
using System.Collections.Generic;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface IArticleTagRepository
    {
        public ArticleTag BuildEntity(int articleId, int tagId);

        public void Create(ArticleTag articleTag);

        public IEnumerable<Article> GetTaggedArticle(int tagId);
    }
}
