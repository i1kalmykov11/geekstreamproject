﻿using GeekStreamProject.Core.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStreamProject.Core.Shared.Abstractions.DataAccess
{
    public interface IUserRepository
    {
        public ApplicationUser GetUserById(string userId);

        public IEnumerable<ApplicationUser> GetUsers();

        public IEnumerable<ApplicationUser> GetAuthors();

        public void Edit(ApplicationUser changedUser);

    }
}
