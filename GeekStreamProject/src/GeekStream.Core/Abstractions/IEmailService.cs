﻿using GeekStreamProject.Core.Shared.Entities;

namespace GeekStream.Core.Abstractions
{
    public interface IEmailService
    {
        public void SendEmail(string userEmail, string content, string body);

        public bool ConfirmUser(string token, string id);
    }
}
