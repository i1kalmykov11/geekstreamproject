﻿using GeekStream.Core.ViewModels;
using System.Collections.Generic;

namespace GeekStream.Core.Abstractions
{
    public interface IArticleService
    {
        public IEnumerable<ArticleViewModel> GetPublishedArticles();

        public IEnumerable<ArticleViewModel> GetNotPublishedUserArticles();
        
        public IEnumerable<ArticleViewModel> GetNotPublishedUserArticles(string userId);

        public IEnumerable<ArticleViewModel> GetNotPublishedArticles();

        public IEnumerable<ArticleViewModel> GetNotApprovedArticles();

        public IEnumerable<ArticleViewModel> GetApprovedArticles();

        public IEnumerable<ArticleViewModel> GetArticlesByCategory(int categoryId);

        public IEnumerable<ArticleViewModel> GetLatestPublishedUserArticles(string userId);

        public IEnumerable<ArticleViewModel> GetSubscriptionArticles(IEnumerable<UserViewModel> subscriptions);

        public ArticleViewModel GetArticle(int id);

        public IEnumerable<ArticleViewModel> SearchArticles(string line);

        public void ApproveArticle(int id);

        public void PublishArticle(int id);

        public void DisApproveArticle(int id);

        public void DisPublishArticle(int id);

        public void Create(CreateArticleViewModel article);

        public void Edit(ArticleViewModel article);

        public void DeleteArticle(int id);

        public void ChangeHonor(int id, int delta);
    }
}
