﻿using GeekStream.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.Abstractions
{
    public interface ISubService
    {
        public IEnumerable<UserViewModel> GetSubscribers();

        public IEnumerable<UserViewModel> GetSubscribers(UserViewModel user);

        public IEnumerable<UserViewModel> GetSubscriptions();

        public IEnumerable<UserViewModel> GetSubscriptions(UserViewModel user);

        public void Subscribe(UserViewModel subscriprion);

        public void Unsubscribe(UserViewModel subscriprion);

        public bool IsSubscribed(string subscriptionId);
    }
}
