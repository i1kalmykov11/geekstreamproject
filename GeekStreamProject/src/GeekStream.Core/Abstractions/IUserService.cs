﻿using GeekStream.Core.ViewModels;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GeekStream.Core.Abstractions
{
    public interface IUserService
    {
        public UserViewModel GetUserById();
        
        public UserViewModel GetUserById(string userId);

        public IEnumerable<UserViewModel> GetUsers();

        public IEnumerable<UserViewModel> GetAuthors();

        public void Edit(UserViewModel changedUser);

        public void ChangePassword(NewPasswordViewModel newPassword);

        public Task<IdentityResult> Register(RegistrationViewModel user);

    }
}
