﻿using GeekStream.Core.ViewModels;
using System.Collections.Generic;

namespace GeekStream.Core.Abstractions
{
    public interface IMessageService
    {
        public void CreateChat(MessangerViewModel chatModel);

        public void CreateChatMessage(MessageViewModel model);

        public void CreateDialogMessage(DialogMessageViewModel dialogMessage);

        public MessangerViewModel GetChat(int id);

        public IEnumerable<MessageViewModel> GetLatestChatMessages(int id);

        public IEnumerable<DialogMessageViewModel> GetLatestDialogMessages(string user2);

        public IEnumerable<MessangerViewModel> GetChats();
    }
}
