﻿using GeekStream.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.Abstractions
{
    public interface ICategoryService
    {
        public IEnumerable<CategoryViewModel> GetCategories();
        public CategoryViewModel GetCategory(int id);
        public void Create(CategoryViewModel model);

    }
}
