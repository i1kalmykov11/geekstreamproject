﻿using GeekStream.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.Abstractions
{
    public interface ICommentService
    {
        public void Create(CreateCommentViewModel commentModel);

        public void ChangeHonor(int id, int delta);
    }
}
