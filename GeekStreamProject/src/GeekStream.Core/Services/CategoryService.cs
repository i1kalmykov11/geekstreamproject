﻿using GeekStream.Core.Abstractions;
using GeekStream.Core.ViewModels;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using System.Collections.Generic;
using System.Linq;

namespace GeekStream.Core.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly ICategoryImageRepository _imageRepository;

        public CategoryService(ICategoryRepository categoryRepository, ICategoryImageRepository imageRepository)
        {
            _categoryRepository = categoryRepository;
            _imageRepository = imageRepository;
        }

        public IEnumerable<CategoryViewModel> GetCategories()
        {
            return _categoryRepository
                .GetCategories()
                .Select(
                    category => new CategoryViewModel
                    {
                        Id = category.Id,
                        Name = category.Name,
                        Image = new ImageCategoryViewModel
                        {
                            Id = category.Image.Id,
                            Path = category.Image.Path,
                            BackGroundPath = category.Image.BackGroundPath,
                            ItemId = category.Id,
                        }
                    });
        }

        public CategoryViewModel GetCategory(int id)
        {
            var category = _categoryRepository.GetCategory(id);

            if (category == null) return null;

            return new CategoryViewModel
            {
                Id = category.Id,
                Name = category.Name,
                Image = new ImageCategoryViewModel
                {
                    Id = category.Image.Id,
                    Path = category.Image.Path,
                    BackGroundPath = category.Image.BackGroundPath,
                    ItemId = category.Id,
                }
            };
        }

        public void Create(CategoryViewModel model)
        {

            var category = new ArticleCategory
            {
                Name = model.Name,
                ImageId = _imageRepository.SaveImage(model.InputImage, model.InputBackGroundImage, "CategoryImages"),
            };

            _categoryRepository.Create(category);
        }   
    }
}
