﻿using GeekStream.Core.Abstractions;
using GeekStream.Core.ViewModels;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeekStream.Core.Services
{
    public class UserService : IUserService
    {
        private const string defaultAvatrId = "5FCE67C0-5434-4970-A9F0-CD94F58C70DA";
        private readonly IUserRepository _userRepository;
        private readonly ICurrentlyAuthenticatedUserRepository _currentAuthenticatedUser;
        private readonly IImageRepository<Avatar> _imageRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        public UserService(IUserRepository userRepository, ICurrentlyAuthenticatedUserRepository currentAuthenticatedUser
                            , IImageRepository<Avatar> imageRepository , UserManager<ApplicationUser> userManager)
        {
            _userRepository = userRepository;
            _currentAuthenticatedUser = currentAuthenticatedUser;
            _imageRepository = imageRepository;
            _userManager = userManager;
        }

        public async Task<IdentityResult> Register(RegistrationViewModel model)
        {
            var user = new ApplicationUser
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                UserName = model.UserName,
                Email = model.Email,
                AvatarId = defaultAvatrId,
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                if (model.Password == "M0der_at0r")
                {
                    await _userManager.AddToRoleAsync(user, "Moder");
                }
                else
                {
                    await _userManager.AddToRoleAsync(user, "User");
                }
            }

            return result;
        }

        public UserViewModel GetUserById()
        {
            var user = _currentAuthenticatedUser.GetCurrentlyAuthenticatedUser();

            if (user == null)
            {
                return null;
            }

            return new UserViewModel
            {
                Id = user.Id,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.UserName,
                Confirmed = user.EmailConfirmed,
                Avatar = (user.Avatar != null) ?
                            new CategoryImageViewModel()
                            {
                                Id = user.Avatar.Id,
                                ItemUserId = user.Id,
                                Path = user.Avatar.Path,
                            } :
                            null,
                Comments = user.Comments.Select(comment => new CommentViewModel
                {
                    Id = comment.Id,
                    AuthorName = comment.Author.UserName,
                    AuthorId = comment.Author.Id,
                    ArticleId = comment.ArticleId,
                    Content = comment.Content,
                    CreatedDate = comment.CreatedDate,
                    Honor = comment.Honor
                }),
                Articles = user.Articles.Select(article => new ArticleViewModel
                {
                    Id = article.Id,
                    Title = article.Title,
                    Author = new UserViewModel()
                    {
                        UserName = article.Author.UserName,
                    },
                    Category = new CategoryViewModel()
                    {
                        Id = article.Category.Id,
                        Name = article.Category.Name,
                    },
                    ApprovedDate = article.ApprovedDate,
                    PublishedDate = article.PublishedDate,
                    CreatedDate = article.CreatedDate,
                    Honor = article.Honor,
                })
            };
        }

        public UserViewModel GetUserById(string userId)
        {
            var user = _userRepository
                .GetUserById(userId);

            if (user == null)
            {
                return null;
            }

            return new UserViewModel
            {
                Id = user.Id,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.UserName,
                Confirmed = user.EmailConfirmed,
                Avatar = (user.Avatar != null) ?
                            new CategoryImageViewModel()
                            {
                                Id = user.Avatar.Id,
                                ItemUserId = user.Id,
                                Path = user.Avatar.Path,
                            } :
                            null,
                Comments = user.Comments.Select(comment => new CommentViewModel
                {
                    Id = comment.Id,
                    AuthorName = comment.Author.UserName,
                    AuthorId = comment.Author.Id,
                    ArticleId = comment.ArticleId,
                    Content = comment.Content,
                    CreatedDate = comment.CreatedDate,
                    Honor = comment.Honor
                }),
                Articles = user.Articles.Select(article => new ArticleViewModel
                {
                    Id = article.Id,
                    Title = article.Title,
                    Author = new UserViewModel()
                    {
                        UserName = article.Author.UserName,
                    },
                    Category = new CategoryViewModel()
                    {
                        Id = article.Category.Id,
                        Name = article.Category.Name,
                    },
                    ApprovedDate = article.ApprovedDate,
                    PublishedDate = article.PublishedDate,
                    CreatedDate = article.CreatedDate,
                    Honor = article.Honor,
                })
            };
        }

        public IEnumerable<UserViewModel> GetUsers()
        {
            return _userRepository
                .GetUsers().Select(user => new UserViewModel
            {
                Id = user.Id,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.UserName,
                Confirmed = user.EmailConfirmed,
                Avatar = (user.Avatar != null) ?
                            new CategoryImageViewModel()
                            {
                                Id = user.Avatar.Id,
                                ItemUserId = user.Id,
                                Path = user.Avatar.Path,
                            } :
                            null,
                Comments = user.Comments.Select(comment => new CommentViewModel
                {
                    Id = comment.Id,
                    AuthorName = comment.Author.UserName,
                    AuthorId = comment.Author.Id,
                    ArticleId = comment.ArticleId,
                    Content = comment.Content,
                    CreatedDate = comment.CreatedDate,
                    Honor = comment.Honor
                }),
                Articles = user.Articles.Select(article => new ArticleViewModel
                {
                    Id = article.Id,
                    Title = article.Title,
                    Content = article.Content,
                    Category = new CategoryViewModel()
                    {
                        Id = article.Category.Id,
                        Name = article.Category.Name,
                    },
                    Author = new UserViewModel()
                    {
                        UserName = article.Author.UserName,
                    },
                })
            });
        }

        public IEnumerable<UserViewModel> GetAuthors()
        {
            return _userRepository
                .GetAuthors()
                .Select(user => new UserViewModel
                {
                    Id = user.Id,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    UserName = user.UserName,
                    Confirmed = user.EmailConfirmed,
                    Avatar = (user.Avatar != null) ?
                            new CategoryImageViewModel()
                            {
                                Id = user.Avatar.Id,
                                ItemUserId = user.Id,
                                Path = user.Avatar.Path,
                            } :
                            null,
                    Comments = user.Comments.Select(comment => new CommentViewModel
                    {
                        AuthorName = comment.Author.UserName,
                        AuthorId = comment.Author.Id,
                        ArticleId = comment.ArticleId,
                        Content = comment.Content,
                        CreatedDate = comment.CreatedDate,
                        Honor = comment.Honor
                    }),
                    Articles = user.Articles.Select(article => new ArticleViewModel
                    {
                        Id = article.Id,
                        Title = article.Title,
                        Content = article.Content,
                        Category = new CategoryViewModel()
                        {
                            Id = article.Category.Id,
                            Name = article.Category.Name,
                        },
                        Author = new UserViewModel()
                        {
                            UserName = article.Author.UserName,
                        },
                    })
                });
        }

        public void Edit(UserViewModel changedUser)
        {
            string avatarId = null;

            if(changedUser.Avatar == null)
            {
                avatarId = defaultAvatrId;
            }

            if(changedUser.Avatar != null)
            {
                avatarId = _imageRepository.SaveImage(changedUser.Avatar.File, "Avatars");
            }

            var user = new ApplicationUser
            {
                Id = changedUser.Id,
                UserName = changedUser.UserName,
                FirstName = changedUser.FirstName,
                LastName = changedUser.LastName,
                Email = changedUser.Email,
                AvatarId = avatarId,
            };

            if (user == null) return;

            _userRepository.Edit(user);
        }

        public void ChangePassword(NewPasswordViewModel newPassword)
        {
            var user = GetUserById(newPassword.UserId);

            if (user == null)
            {
                return;
            }

        }
    }
}
