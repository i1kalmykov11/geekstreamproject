﻿using GeekStream.Core.Abstractions;
using GeekStream.Core.ViewModels;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using System.Collections.Generic;
using System.Linq;

namespace GeekStream.Core.Services
{
    public class SubService : ISubService
    {
        private readonly ISubcriptionRepository _subRepository;
        private readonly ICurrentlyAuthenticatedUserRepository _currentAuthenticatedUser;

        public SubService(ISubcriptionRepository subRepository, ICurrentlyAuthenticatedUserRepository currentAuthenticatedUser)
        {
            _subRepository = subRepository;
            _currentAuthenticatedUser = currentAuthenticatedUser;
        }

        public IEnumerable<UserViewModel> GetSubscribers()
        {
            var user = _currentAuthenticatedUser.GetCurrentlyAuthenticatedUser();

            if (user == null)
            {
                return null;
            }

            return _subRepository
                        .GetSubscribers(user.Id)
                        .Select(sub => new UserViewModel
                        {
                            Id = sub.Id,
                            UserName = sub.UserName,
                            Avatar = new CategoryImageViewModel()
                            {
                                Path = sub.Avatar.Path,
                            },
                            Comments = sub.Comments.Select(comment => new CommentViewModel
                            {
                                AuthorName = comment.Author.UserName,
                                AuthorId = comment.Author.Id,
                                ArticleId = comment.ArticleId,
                                Content = comment.Content,
                                CreatedDate = comment.CreatedDate,
                                Honor = comment.Honor
                            }),
                            Articles = sub.Articles.Select(article => new ArticleViewModel
                            {
                                Id = article.Id,
                                Title = article.Title,
                                Content = article.Content,
                                Category = new CategoryViewModel()
                                {
                                    Id = article.Category.Id,
                                    Name = article.Category.Name,
                                },
                                Author = new UserViewModel()
                                {
                                    UserName = article.Author.UserName,
                                },
                            })
                        });
        }

        public IEnumerable<UserViewModel> GetSubscriptions()
        {
            var user =  _currentAuthenticatedUser.GetCurrentlyAuthenticatedUser();

            if (user == null)
            {
                return null;
            }

            return _subRepository
                        .GetSubscriptions(user.Id)
                        .Select(sub => new UserViewModel
                        {
                            Id = sub.Id,
                            Email = sub.Email,
                            FirstName = sub.FirstName,
                            LastName = sub.LastName,
                            UserName = sub.UserName,
                            Confirmed = sub.EmailConfirmed,
                            Avatar = (sub.Avatar != null) ?
                            new CategoryImageViewModel()
                            {
                                Id = sub.Avatar.Id,
                                ItemUserId = sub.Id,
                                Path = sub.Avatar.Path,
                            } :
                            null,
                            Comments = sub.Comments.Select(comment => new CommentViewModel
                            {
                                Id = comment.Id,
                                AuthorName = comment.Author.UserName,
                                AuthorId = comment.Author.Id,
                                ArticleId = comment.ArticleId,
                                Content = comment.Content,
                                CreatedDate = comment.CreatedDate,
                                Honor = comment.Honor
                            }),
                            Articles = sub.Articles.Select(article => new ArticleViewModel
                            {
                                Id = article.Id,
                                Title = article.Title,
                                Author = new UserViewModel()
                                {
                                    UserName = article.Author.UserName,
                                },
                                Category = new CategoryViewModel()
                                {
                                    Id = article.Category.Id,
                                    Name = article.Category.Name,
                                },
                                ApprovedDate = article.ApprovedDate,
                                PublishedDate = article.PublishedDate,
                                CreatedDate = article.CreatedDate,
                                Honor = article.Honor,
                            })
                        });
        }

        public IEnumerable<UserViewModel> GetSubscribers(UserViewModel user)
        {
            return _subRepository
                        .GetSubscribers(user.Id)
                        .Select(sub => new UserViewModel
                        {
                            Id = sub.Id,
                            Email = sub.Email,
                            FirstName = sub.FirstName,
                            LastName = sub.LastName,
                            UserName = sub.UserName,
                            Confirmed = sub.EmailConfirmed,
                            Avatar = (sub.Avatar != null) ?
                            new CategoryImageViewModel()
                            {
                                Id = sub.Avatar.Id,
                                ItemUserId = sub.Id,
                                Path = sub.Avatar.Path,
                            } :
                            null,
                            Comments = sub.Comments.Select(comment => new CommentViewModel
                            {
                                Id = comment.Id,
                                AuthorName = comment.Author.UserName,
                                AuthorId = comment.Author.Id,
                                ArticleId = comment.ArticleId,
                                Content = comment.Content,
                                CreatedDate = comment.CreatedDate,
                                Honor = comment.Honor
                            }),
                            Articles = sub.Articles.Select(article => new ArticleViewModel
                            {
                                Id = article.Id,
                                Title = article.Title,
                                Author = new UserViewModel()
                                {
                                    UserName = article.Author.UserName,
                                },
                                Category = new CategoryViewModel()
                                {
                                    Id = article.Category.Id,
                                    Name = article.Category.Name,
                                },
                                ApprovedDate = article.ApprovedDate,
                                PublishedDate = article.PublishedDate,
                                CreatedDate = article.CreatedDate,
                                Honor = article.Honor,
                            })
                        });
        }

        public IEnumerable<UserViewModel> GetSubscriptions(UserViewModel user)
        {
            return _subRepository
                        .GetSubscriptions(user.Id)
                        .Select(sub => new UserViewModel
                        {
                            Id = sub.Id,
                            Email = sub.Email,
                            FirstName = sub.FirstName,
                            LastName = sub.LastName,
                            UserName = sub.UserName,
                            Confirmed = sub.EmailConfirmed,
                            Avatar = (sub.Avatar != null) ?
                            new CategoryImageViewModel()
                            {
                                Id = sub.Avatar.Id,
                                ItemUserId = sub.Id,
                                Path = sub.Avatar.Path,
                            } :
                            null,
                            Comments = sub.Comments.Select(comment => new CommentViewModel
                            {
                                Id = comment.Id,
                                AuthorName = comment.Author.UserName,
                                AuthorId = comment.Author.Id,
                                ArticleId = comment.ArticleId,
                                Content = comment.Content,
                                CreatedDate = comment.CreatedDate,
                                Honor = comment.Honor
                            }),
                            Articles = sub.Articles.Select(article => new ArticleViewModel
                            {
                                Id = article.Id,
                                Title = article.Title,
                                Author = new UserViewModel()
                                {
                                    UserName = article.Author.UserName,
                                },
                                Category = new CategoryViewModel()
                                {
                                    Id = article.Category.Id,
                                    Name = article.Category.Name,
                                },
                                ApprovedDate = article.ApprovedDate,
                                PublishedDate = article.PublishedDate,
                                CreatedDate = article.CreatedDate,
                                Honor = article.Honor,
                            })
                        });
        }

        public void Subscribe(UserViewModel subscription)
        {
            var subscriber = _currentAuthenticatedUser.GetCurrentlyAuthenticatedUser();

            if (subscriber == null) return;

            if (subscription == null) return;
            
            _subRepository
                .Subscribe(subscriber.Id, subscription.Id);
        }

        public void Unsubscribe(UserViewModel subscription)
        {
            var subscriber = _currentAuthenticatedUser.GetCurrentlyAuthenticatedUser();

            if (subscriber == null) return;

            if (subscription == null) return;

            _subRepository
                .Unsubscribe(subscriber.Id, subscription.Id);
        }

        public bool IsSubscribed(string subscriptionId)
        {
            var subscriber = _currentAuthenticatedUser.GetCurrentlyAuthenticatedUser();

            if (subscriber == null) return false;

            return (_subRepository.IsSubscribed(subscriber.Id, subscriptionId) == true) ?
                    true : false;
        }
    }
}
