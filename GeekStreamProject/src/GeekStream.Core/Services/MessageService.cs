﻿using GeekStream.Core.Abstractions;
using GeekStream.Core.ViewModels;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace GeekStream.Core.Services
{
    public class MessageService : IMessageService
    {
        private readonly IMessageRepository _chatRepository;
        private readonly ICurrentlyAuthenticatedUserRepository _currentAuthenticatedUser;
        private readonly UserManager<ApplicationUser> _userManager;

        public MessageService(IMessageRepository chatRepository, ICurrentlyAuthenticatedUserRepository currentAuthenticatedUser, UserManager<ApplicationUser> userManager)
        {
            _chatRepository = chatRepository;
            _currentAuthenticatedUser = currentAuthenticatedUser;
            _userManager = userManager;
        }

        public void CreateChat(MessangerViewModel chatModel)
        {
            var chat = new Chat()
            {
                Name = chatModel.Name,
            };

            _chatRepository
                .CreateChat(chat);
        }

        public void CreateChatMessage(MessageViewModel model)
        {
            var message = new ChatMessage
            {
                AuthorId = _currentAuthenticatedUser.GetCurrentlyAuthenticatedUser().Id,
                Content = model.Content,
                ChatId = model.ChatId,
            };
            _chatRepository.CreateChatMessage(message);
        }

        public void CreateDialogMessage(DialogMessageViewModel model) 
        {
            var user = _currentAuthenticatedUser.GetCurrentlyAuthenticatedUser();

            if (user == null) return;

            var message = new DialogMessage
            {
                AuthorId = user.Id,
                RecieverId = _userManager.FindByNameAsync(model.ReceiverName).GetAwaiter().GetResult().Id,
                Content = model.Content
            };
            _chatRepository.CreateDialogMessage(message);
        }

        public MessangerViewModel GetChat(int id)
        {
            var chat = _chatRepository.GetChat(id);

            if (chat == null)
            {
                return null;
            }

            return new MessangerViewModel()
            {
                Id = chat.Id,
                Name = chat.Name,
                Messages = chat.Messages.Select(message => new MessageViewModel()
                {
                    Id = message.Id,
                    Content = message.Content,
                    AuthorName = message.Author.UserName,
                    SendedTime = message.SendedTime,
                }),
            };
        }

        public IEnumerable<MessageViewModel> GetLatestChatMessages(int id)
        {
            return _chatRepository.GetLatestChatMessages(id)
                .Select(message => new MessageViewModel
                {
                    AuthorName = message.Author.UserName,
                    Content = message.Content,
                    Id = message.Id,
                    SendedTime = message.SendedTime
                });
        }

        public IEnumerable<DialogMessageViewModel> GetLatestDialogMessages(string user2Name)
        {
            var user1 = _currentAuthenticatedUser.GetCurrentlyAuthenticatedUser();

            if (user1 == null) return null;

            var user2Id = _userManager.FindByNameAsync(user2Name).GetAwaiter().GetResult().Id;
            return _chatRepository
                .GetLatestDialogMessages(user1.Id, user2Id)
                .Select(message => new DialogMessageViewModel
                {
                    AuthorName = message.Author.UserName,
                    ReceiverName = message.Reciever.UserName,
                    Content = message.Content,
                    Id = message.Id,
                    SendedTime = message.SendedTime,
                    ReadedTime = message.ReadedTime
                });
        }

        public IEnumerable<MessangerViewModel> GetChats()
        {
            return _chatRepository.GetChats().Select(chat => new MessangerViewModel()
                {
                    Id = chat.Id,
                    Name = chat.Name,
                    Messages = chat.Messages.Select(message => new MessageViewModel()
                    {
                        Id = message.Id,
                        AuthorName = message.AuthorId,
                        Content = message.Content,
                        SendedTime = message.SendedTime,
                    })
                });
        }
    }
}
