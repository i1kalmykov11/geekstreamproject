﻿using GeekStream.Core.Abstractions;
using GeekStream.Core.ViewModels;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.Services
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;
        private readonly ICurrentlyAuthenticatedUserRepository _currentAuthenticatedUser;

        public CommentService(ICommentRepository commentRepository, ICurrentlyAuthenticatedUserRepository currentAuthenticatedUser)
        {
            _commentRepository = commentRepository;
            _currentAuthenticatedUser = currentAuthenticatedUser;
        }

        public void Create(CreateCommentViewModel commentModel)
        {
            var user = _currentAuthenticatedUser.GetCurrentlyAuthenticatedUser();

            if (user == null) return;

            var comment = new Comment
            {
                AuthorId = user.Id,
                ArticleId = commentModel.ArticleId,
                Content = commentModel.Content,
                CreatedDate = DateTime.UtcNow,
                Honor = 0
            };

            _commentRepository.Create(comment);
        }

        public void ChangeHonor(int id, int delta)
        {
            _commentRepository.ChangeHonor(id, delta);
        }
    }
}
