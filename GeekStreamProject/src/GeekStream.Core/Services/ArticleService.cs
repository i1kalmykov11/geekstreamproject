﻿using GeekStream.Core.Abstractions;
using GeekStream.Core.ViewModels;
using GeekStreamProject.Core.Shared;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace GeekStream.Core.Services
{
    public class ArticleService : IArticleService
    {
        private readonly IArticleRepository _articleRepository;
        private readonly ITagRepository _tagRepository;
        private readonly IArticleTagRepository _articleTagRepository;
        private readonly ICurrentlyAuthenticatedUserRepository _currentAuthenticatedUser;
        private readonly IImageRepository<ArticleImage> _imageRepository;

        public ArticleService(IArticleRepository articleRepository, ICurrentlyAuthenticatedUserRepository currentAuthenticatedUser
                                , IImageRepository<ArticleImage> imageRepository, ITagRepository tagRepository
                                , IArticleTagRepository articleTagRepository)
        {
            _articleRepository = articleRepository;
            _currentAuthenticatedUser = currentAuthenticatedUser;
            _imageRepository = imageRepository;
            _tagRepository = tagRepository;
            _articleTagRepository = articleTagRepository;
        }

        public IEnumerable<ArticleViewModel> GetPublishedArticles()
        {
            return _articleRepository
                .GetPublishedArticles()
                .Select(article => new ArticleViewModel
                {
                    Id = article.Id,
                    Title = article.Title,
                    Content = article.Content,
                    Category = new CategoryViewModel()
                    {
                        Id = article.Category.Id,
                        Name = article.Category.Name,
                    },
                    ApprovedDate = article.ApprovedDate,
                    PublishedDate = article.PublishedDate,
                    CreatedDate = article.CreatedDate,
                    Image = new CategoryImageViewModel()
                    {
                        Id = article.Image.Id,
                        ItemId = article.Id,
                        Path = article.Image.Path,
                    },
                    Comments = article.Comments.Select( comment => new CommentViewModel
                        {
                            Id = comment.Id,
                            AuthorName = comment.Author.UserName,
                            AuthorId = comment.Author.Id,
                            Content = comment.Content,
                            CreatedDate = comment.CreatedDate,
                            Honor = comment.Honor,
                        }),
                    Honor = article.Honor,
                    Author = new UserViewModel()
                    {
                        FirstName = article.Author.FirstName,
                        LastName = article.Author.LastName,
                        UserName = article.Author.UserName,
                        Avatar = new CategoryImageViewModel()
                        {
                            Path = article.Author.Avatar.Path,
                        }
                    },
                    Tags = _tagRepository.TagToString(article.ArticleTags.Select(tag => tag.Tag.Content)),
                }) ;
        }

        public IEnumerable<ArticleViewModel> GetNotPublishedUserArticles()
        {
            var user = _currentAuthenticatedUser.GetCurrentlyAuthenticatedUser();

            if (user == null) return null;

            return _articleRepository
                .GetNotPublishedUserArticles(user.Id)
                .Select(article => new ArticleViewModel
                {
                    Id = article.Id,
                    Title = article.Title,
                    Content = article.Content,
                    Category = new CategoryViewModel()
                    {
                        Id = article.Category.Id,
                        Name = article.Category.Name,
                    },
                    ApprovedDate = article.ApprovedDate,
                    PublishedDate = article.PublishedDate,
                    CreatedDate = article.CreatedDate,
                    Honor = article.Honor,
                    Image = new CategoryImageViewModel()
                    {
                        Id = article.Image.Id,
                        ItemId = article.Id,
                        Path = article.Image.Path,
                    },
                    Author = new UserViewModel()
                    {
                        FirstName = article.Author.FirstName,
                        LastName = article.Author.LastName,
                        UserName = article.Author.UserName,
                        Avatar = new CategoryImageViewModel()
                        {
                            Path = article.Author.Avatar.Path,
                        }
                    },
                    Comments = article.Comments.Select(comment => new CommentViewModel
                    {
                        Id = comment.Id,
                        AuthorName = comment.Author.UserName,
                        AuthorId = comment.Author.Id,
                        Content = comment.Content,
                        CreatedDate = comment.CreatedDate,
                        Honor = comment.Honor,
                    }),
                    Tags = _tagRepository.TagToString(article.ArticleTags.Select(tag => tag.Tag.Content)),

                });
        }

        public IEnumerable<ArticleViewModel> GetNotPublishedUserArticles(string userId)
        {
            return _articleRepository
                .GetNotPublishedUserArticles(userId)
                .Select(article => new ArticleViewModel
                {
                    Id = article.Id,
                    Title = article.Title,
                    Content = article.Content,
                    Category = new CategoryViewModel()
                    {
                        Id = article.Category.Id,
                        Name = article.Category.Name,
                    },
                    ApprovedDate = article.ApprovedDate,
                    PublishedDate = article.PublishedDate,
                    CreatedDate = article.CreatedDate,
                    Honor = article.Honor,
                    Image = new CategoryImageViewModel()
                    {
                        Id = article.Image.Id,
                        ItemId = article.Id,
                        Path = article.Image.Path,
                    },
                    Author = new UserViewModel()
                    {
                        FirstName = article.Author.FirstName,
                        LastName = article.Author.LastName,
                        UserName = article.Author.UserName,
                        Avatar = new CategoryImageViewModel()
                        {
                            Path = article.Author.Avatar.Path,
                        }
                    },
                    Comments = article.Comments.Select(comment => new CommentViewModel
                    {
                        Id = comment.Id,
                        AuthorName = comment.Author.UserName,
                        AuthorId = comment.Author.Id,
                        Content = comment.Content,
                        CreatedDate = comment.CreatedDate,
                        Honor = comment.Honor,
                    }),
                    Tags = _tagRepository.TagToString(article.ArticleTags.Select(tag => tag.Tag.Content)),
                });
        }

        public IEnumerable<ArticleViewModel> GetNotPublishedArticles()
        {
            return _articleRepository
                .GetNotPublishedArticles()
                .Select(article => new ArticleViewModel
                {
                    Id = article.Id,
                    Title = article.Title,
                    Content = article.Content,
                    Category = new CategoryViewModel()
                    {
                        Id = article.Category.Id,
                        Name = article.Category.Name,
                    },
                    ApprovedDate = article.ApprovedDate,
                    PublishedDate = article.PublishedDate,
                    CreatedDate = article.CreatedDate,
                    Honor = article.Honor,
                    Image = new CategoryImageViewModel()
                    {
                        Id = article.Image.Id,
                        ItemId = article.Id,
                        Path = article.Image.Path,
                    },
                    Author = new UserViewModel()
                    {
                        FirstName = article.Author.FirstName,
                        LastName = article.Author.LastName,
                        UserName = article.Author.UserName,
                        Avatar = new CategoryImageViewModel()
                        {
                            Path = article.Author.Avatar.Path,
                        }
                    },
                    Comments = article.Comments.Select(comment => new CommentViewModel
                    {
                        Id = comment.Id,
                        AuthorName = comment.Author.UserName,
                        AuthorId = comment.Author.Id,
                        Content = comment.Content,
                        CreatedDate = comment.CreatedDate,
                        Honor = comment.Honor,
                    }),
                    Tags = _tagRepository.TagToString(article.ArticleTags.Select(tag => tag.Tag.Content)),
                });
        }

        public IEnumerable<ArticleViewModel> GetNotApprovedArticles()
        {
            return _articleRepository
                .GetNotApprovedArticles()
                .Select(article => new ArticleViewModel
                {
                    Id = article.Id,
                    Title = article.Title,
                    Content = article.Content,
                    Category = new CategoryViewModel()
                    {
                        Id = article.Category.Id,
                        Name = article.Category.Name,
                    },
                    ApprovedDate = article.ApprovedDate,
                    PublishedDate = article.PublishedDate,
                    CreatedDate = article.CreatedDate,
                    Honor = article.Honor,
                    Author = new UserViewModel()
                    {
                        FirstName = article.Author.FirstName,
                        LastName = article.Author.LastName,
                        UserName = article.Author.UserName,
                        Avatar = new CategoryImageViewModel()
                        {
                            Path = article.Author.Avatar.Path,
                        }
                    },
                    Image = new CategoryImageViewModel()
                    {
                        Id = article.Image.Id,
                        ItemId = article.Id,
                        Path = article.Image.Path,
                    },
                    Comments = article.Comments.Select(comment => new CommentViewModel
                    {
                        Id = comment.Id,
                        AuthorName = comment.Author.UserName,
                        AuthorId = comment.Author.Id,
                        Content = comment.Content,
                        CreatedDate = comment.CreatedDate,
                        Honor = comment.Honor,
                    }),
                    Tags = _tagRepository.TagToString(article.ArticleTags.Select(tag => tag.Tag.Content)),
                });
        }

        public IEnumerable<ArticleViewModel> GetApprovedArticles()
        {
            return _articleRepository
                .GetApprovedArticles()
                .Select(article => new ArticleViewModel
                {
                    Id = article.Id,
                    Title = article.Title,
                    Content = article.Content,
                    Category = new CategoryViewModel()
                    {
                        Id = article.Category.Id,
                        Name = article.Category.Name,
                    },
                    ApprovedDate = article.ApprovedDate,
                    PublishedDate = article.PublishedDate,
                    CreatedDate = article.CreatedDate,
                    Honor = article.Honor,
                    Author = new UserViewModel()
                    {
                        FirstName = article.Author.FirstName,
                        LastName = article.Author.LastName,
                        UserName = article.Author.UserName,
                        Avatar = new CategoryImageViewModel()
                        {
                            Path = article.Author.Avatar.Path,
                        }
                    },
                    Image = new CategoryImageViewModel()
                    {
                        Id = article.Image.Id,
                        ItemId = article.Id,
                        Path = article.Image.Path,
                    },
                    Comments = article.Comments.Select(comment => new CommentViewModel
                    {
                        Id = comment.Id,
                        AuthorName = comment.Author.UserName,
                        AuthorId = comment.Author.Id,
                        Content = comment.Content,
                        CreatedDate = comment.CreatedDate,
                        Honor = comment.Honor,
                    }),
                    Tags = _tagRepository.TagToString(article.ArticleTags.Select(tag => tag.Tag.Content)),
                });
        }

        public ArticleViewModel GetArticle(int id)
        {
            var article = _articleRepository.GetArticleInfo(id);
            
            if (article == null)
            {
                return null;
            }

            return new ArticleViewModel
            {
                Id = article.Id,
                Title = article.Title,
                Content = article.Content,
                Category = new CategoryViewModel()
                {
                    Id = article.Category.Id,
                    Name = article.Category.Name,
                },
                ApprovedDate = article.ApprovedDate,
                PublishedDate = article.PublishedDate,
                CreatedDate = article.CreatedDate,
                Image = new CategoryImageViewModel()
                {
                    Id = article.Image.Id,
                    ItemId = article.Id,
                    Path = article.Image.Path,
                },
                Comments = article.Comments
                    .Select(comment => new CommentViewModel
                    {
                        Id = comment.Id,
                        AuthorName = comment.Author.UserName,
                        AuthorId = comment.Author.Id,
                        ArticleId = article.Id,
                        Content = comment.Content,
                        CreatedDate = comment.CreatedDate,
                        Honor = comment.Honor,
                    }),
                Honor = article.Honor,
                Author = new UserViewModel()
                {
                    Id = article.Author.Id,
                    FirstName = article.Author.FirstName,
                    LastName = article.Author.LastName,
                    UserName = article.Author.UserName,
                    Avatar = new CategoryImageViewModel()
                    {
                        Path = article.Author.Avatar.Path,
                    }
                },
                Tags = _tagRepository.TagToString(article.ArticleTags.Select(tag => tag.Tag.Content)),
            };
        }

        public IEnumerable<ArticleViewModel> GetArticlesByCategory(int categoryId)
        {
            return _articleRepository.GetPublishedArticlesByCategory(categoryId).Select(article => new ArticleViewModel
            {
                Id = article.Id,
                Title = article.Title,
                Content = article.Content,
                Category = new CategoryViewModel()
                {
                    Id = article.Category.Id,
                    Name = article.Category.Name,
                },
                ApprovedDate = article.ApprovedDate,
                PublishedDate = article.PublishedDate,
                CreatedDate = article.CreatedDate,
                Honor = article.Honor,
                Author = new UserViewModel()
                {
                    FirstName = article.Author.FirstName,
                    LastName = article.Author.LastName,
                    UserName = article.Author.UserName,
                    Avatar = new CategoryImageViewModel()
                    {
                        Path = article.Author.Avatar.Path,
                    }
                },
                Image = new CategoryImageViewModel()
                {
                    Id = article.Image.Id,
                    ItemId = article.Id,
                    Path = article.Image.Path,
                },
                Comments = article.Comments.Select(comment => new CommentViewModel
                {
                    Id = comment.Id,
                    AuthorName = comment.Author.UserName,
                    AuthorId = comment.Author.Id,
                    Content = comment.Content,
                    CreatedDate = comment.CreatedDate,
                    Honor = comment.Honor,
                }),
                Tags = _tagRepository.TagToString(article.ArticleTags.Select(tag => tag.Tag.Content)),
            });
        }

        public IEnumerable<ArticleViewModel> GetLatestPublishedUserArticles(string userId)
        {
            return _articleRepository
                    .GetLatestPublishedUserArticles(userId)
                    .Select(article => new ArticleViewModel
                    {
                        Id = article.Id,
                        Title = article.Title,
                        Content = article.Content,
                        Category = new CategoryViewModel()
                        {
                            Id = article.Category.Id,
                            Name = article.Category.Name,
                        },
                        ApprovedDate = article.ApprovedDate,
                        PublishedDate = article.PublishedDate,
                        CreatedDate = article.CreatedDate,
                        Honor = article.Honor,
                        Author = new UserViewModel()
                        {
                            FirstName = article.Author.FirstName,
                            LastName = article.Author.LastName,
                            UserName = article.Author.UserName,
                            Avatar = new CategoryImageViewModel()
                            {
                                Path = article.Author.Avatar.Path,
                            }
                        },
                        Image = new CategoryImageViewModel()
                        {
                            Id = article.Image.Id,
                            ItemId = article.Id,
                            Path = article.Image.Path,
                        },
                        Comments = article.Comments.Select(comment => new CommentViewModel
                        {
                            Id = comment.Id,
                            AuthorName = comment.Author.UserName,
                            AuthorId = comment.Author.Id,
                            Content = comment.Content,
                            CreatedDate = comment.CreatedDate,
                            Honor = comment.Honor,
                        }),
                        Tags = _tagRepository.TagToString(article.ArticleTags.Select(tag => tag.Tag.Content)),

                    });
        }

        public IEnumerable<ArticleViewModel> GetSubscriptionArticles(IEnumerable<UserViewModel> subscriptions)
        {
            List<ArticleViewModel> articles = new List<ArticleViewModel>();
            foreach (var sub in subscriptions)
            {
                articles.AddRange(GetLatestPublishedUserArticles(sub.Id));
            }

            if (articles == null)
            {
                return null;
            }

            return articles;
        }

        public IEnumerable<ArticleViewModel> SearchArticles(string line)
        {
            var tags = line.Split(" ");

            if (tags == null)
            {
                return null;
            }

            List<int> tagsId = new List<int>();

            foreach (var tag in tags)
            {
                tagsId.Add(_tagRepository.GetTag(tag).Id);
            }

            List<Article> articles = new List<Article>();

            foreach (var ids in tagsId)
            {
                articles.AddRange(_articleTagRepository.GetTaggedArticle(ids));
            }

            return articles.Select(article => new ArticleViewModel
            {
                Id = article.Id,
                Title = article.Title,
                Content = article.Content,
                Category = new CategoryViewModel()
                {
                    Id = article.Category.Id,
                    Name = article.Category.Name,
                },
                ApprovedDate = article.ApprovedDate,
                PublishedDate = article.PublishedDate,
                CreatedDate = article.CreatedDate,
                Honor = article.Honor,
                Author = new UserViewModel()
                {
                    FirstName = article.Author.FirstName,
                    LastName = article.Author.LastName,
                    UserName = article.Author.UserName,
                    Avatar = new CategoryImageViewModel()
                    {
                        Path = article.Author.Avatar.Path,
                    }
                },
                Image = new CategoryImageViewModel()
                {
                    Id = article.Image.Id,
                    ItemId = article.Id,
                    Path = article.Image.Path,
                },
                Comments = article.Comments.Select(comment => new CommentViewModel
                {
                    Id = comment.Id,
                    AuthorName = comment.Author.UserName,
                    AuthorId = comment.Author.Id,
                    Content = comment.Content,
                    CreatedDate = comment.CreatedDate,
                    Honor = comment.Honor,
                }),
                Tags = _tagRepository.TagToString(article.ArticleTags.Select(tag => tag.Tag.Content)),
            });
        }

        public void ApproveArticle(int id)
        {
            _articleRepository
                .ApproveArticle(id);
        }

        public void PublishArticle(int id)
        {
            _articleRepository
                .PublishArticle(id);
        }

        public void DisApproveArticle(int id)
        {
            _articleRepository
                .DisApproveArticle(id);
        }

        public void DisPublishArticle(int id)
        {
            _articleRepository
                .DisPublishArticle(id);
        }

        public void Create(CreateArticleViewModel model)
        {
            var user = _currentAuthenticatedUser.GetCurrentlyAuthenticatedUser();

            if (user == null) return;

            var article = new Article()
            {
                Title = model.Title,
                Content = model.Content,
                CategoryId = model.CategoryId,
                AuthorId = user.Id,
                Comments = null,
                Honor = 0,
                ImageId = (model.File != null) ? _imageRepository.SaveImage(model.File, "ArticleImages") : null,
            };

            _articleRepository.Create(article);

            var line = model.Tags.Split(" ");
            _tagRepository.CreateTagsFromStringInput(line);

            foreach(var tag in line)
            {
                var tagId = _tagRepository.GetTag(tag).Id;
                var tagArticle = _articleTagRepository.BuildEntity(article.Id, tagId);
                _articleTagRepository.Create(tagArticle);
            };
        }

        public void Edit(ArticleViewModel model)
        {
            var user = _currentAuthenticatedUser.GetCurrentlyAuthenticatedUser();

            if (user == null) return;

            var changedArticle = new Article()
            {
                Id = model.Id,
                Title = model.Title,
                Content = model.Content,
                CategoryId = model.Category.Id,
                AuthorId = user.Id,
                ImageId = (model.Image.File != null) ? _imageRepository.SaveImage(model.Image.File, "ArticleImages") : null,
                CreatedDate = model.CreatedDate,
            };

            _articleRepository.Edit(changedArticle);
        }

        public void DeleteArticle(int id)
        {
            var article = GetArticle(id);

            if (article != null)
            {
                _imageRepository.Delete(article.Image.Id);
            }

            _articleRepository.Delete(id);
        }

        public void ChangeHonor(int id, int delta)
        {
            _articleRepository.ChangeHonor(id, delta);
        }
    }
}
