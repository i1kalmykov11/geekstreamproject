﻿using GeekStreamProject.Core.Shared.Entities;
using Microsoft.AspNetCore.Identity;
using MimeKit;
using MailKit.Net.Smtp;
using GeekStream.Core.Abstractions;
using Microsoft.Extensions.Configuration;

namespace GeekStream.Core.Services
{
    public class EmailService : IEmailService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;

        public EmailService(UserManager<ApplicationUser> userManager, IConfiguration configuration)
        {
            _userManager = userManager;
            _configuration = configuration;
        }

        public void SendEmail(string userEmail, string subject, string body)
        {
            var message = new MimeMessage();

            message.From.Add(new MailboxAddress("VantusCorp", _configuration["OfficeInfo:Mail"]));
            message.To.Add(new MailboxAddress("", userEmail));

            message.Subject = subject;

            message.Body = new TextPart("plain")
            {
                Text = body
            };

            using (var smtpClient = new SmtpClient())
            {
                smtpClient.Connect("smtp.gmail.com", 587, false);
                smtpClient.Authenticate(_configuration["OfficeInfo:Mail"], _configuration["OfficeInfo:Password"]);
                smtpClient.Send(message);
                smtpClient.Disconnect(true);
            }
        }

        public bool ConfirmUser(string token, string id)
        {
            var user = _userManager.FindByIdAsync(id).GetAwaiter().GetResult();

            if(user == null)
            {
                return false;
            }

            var result =  _userManager.ConfirmEmailAsync(user, token).GetAwaiter().GetResult();

            return result.Succeeded;
        }
    }
}
