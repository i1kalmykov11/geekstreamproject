﻿namespace GeekStream.Core.Additional
{
    public class UrlBack
    {
        public string Controller { get; set; }

        public string Action { get; set; }

        public int? Int_key { get; set; }

        public string? Str_key { get; set; }
    }
}
