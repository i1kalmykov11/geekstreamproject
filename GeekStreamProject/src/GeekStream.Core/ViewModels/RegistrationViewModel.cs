﻿using Microsoft.AspNetCore.Http;

namespace GeekStream.Core.ViewModels
{
    public class RegistrationViewModel
    {
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public string UserName { get; set; }

        public string Email { get; set; }

        public IFormFile Avatar { get; set; }
        
        public string Password { get; set; }
        
        public string ConfirmPassword { get; set; }
    }
}
