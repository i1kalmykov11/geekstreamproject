﻿namespace GeekStream.Core.ViewModels
{
    public class NewPasswordViewModel
    {
        public string Token { get; set; }

        public string UserId { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }
}
