﻿using System.Collections.Generic;

namespace GeekStream.Core.ViewModels
{
    public class MessangerViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<MessageViewModel> Messages { get; set; }
    }
}
