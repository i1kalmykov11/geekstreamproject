﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.ViewModels
{
    public class DialogMessageViewModel : MessageViewModel
    { 
        public string ReceiverName { get; set; }

        public DateTime ReadedTime { get; set; }
    }
}
