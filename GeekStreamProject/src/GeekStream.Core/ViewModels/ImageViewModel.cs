﻿using Microsoft.AspNetCore.Http;

namespace GeekStream.Core.ViewModels
{
    public class CategoryImageViewModel
    {
        public string Id { get; set; }

        public IFormFile File { get; set; }

        public string Path { get; set; }

        public int? ItemId { get; set; }

        public string? ItemUserId { get; set; }
    }
}
