﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GeekStream.Core.ViewModels
{
    public class MessageViewModel
    {
        public int Id { get; set; }

        public string AuthorName { get; set; }

        public DateTime SendedTime { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Content { get; set; }

        public int ChatId { get; set; }
    }
}
