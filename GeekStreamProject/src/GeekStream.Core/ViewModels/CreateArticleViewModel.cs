﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace GeekStream.Core.ViewModels
{
    public class CreateArticleViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public IFormFile File { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CategoryId { get; set; }

        public string Tags { get; set; }
        
    }
}
