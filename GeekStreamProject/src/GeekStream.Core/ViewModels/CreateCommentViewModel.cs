﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.ViewModels
{
    public class CreateCommentViewModel
    {
        public int ArticleId { get; set; }

        public DateTime CreatedDate { get; set; } 

        public string Content { get; set; }
    }
}
