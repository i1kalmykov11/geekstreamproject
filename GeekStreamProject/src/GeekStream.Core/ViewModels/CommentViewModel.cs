﻿using System;

namespace GeekStream.Core.ViewModels
{
    public class CommentViewModel
    {
        public int Id { get; set; }

        public string AuthorName { get; set; }

        public string AuthorId { get; set; }

        public int ArticleId { get; set; }

        public string Content { get; set; }

        public DateTime CreatedDate { get; set; }

        public int Honor { get; set; }
    }
}
