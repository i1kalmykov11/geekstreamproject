﻿using System;
using System.Collections.Generic;

namespace GeekStream.Core.ViewModels
{
    public class ArticleViewModel
    {
        
        public int Id { get; set; }
       
        public string Title { get; set; }
        
        public string Content { get; set; }

        public CategoryImageViewModel Image { get; set; }

        public DateTime? PublishedDate { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public CategoryViewModel Category { get; set; }
        
        public UserViewModel Author { get; set; }

        public int Honor { get; set; }

        public IEnumerable<CommentViewModel> Comments { get; set; }

        public string Tags { get; set; }
    }
}
