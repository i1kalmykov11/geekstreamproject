﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.ViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }

        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool Confirmed { get; set; }

        public CategoryImageViewModel? Avatar { get; set; }

        public IEnumerable<ArticleViewModel>? Articles { get; set; }

        public IEnumerable<CommentViewModel>? Comments { get; set; }
    }
}
