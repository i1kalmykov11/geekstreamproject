﻿using Microsoft.AspNetCore.Http;

namespace GeekStream.Core.ViewModels
{
    public class CategoryViewModel
    {
        public int Id { get; set; }

        public IFormFile InputImage { get; set; }

        public ImageCategoryViewModel Image { get; set; }

        public IFormFile InputBackGroundImage { get; set; }


        public string Name { get; set; }

        public string Description { get; set; }
    }
}
