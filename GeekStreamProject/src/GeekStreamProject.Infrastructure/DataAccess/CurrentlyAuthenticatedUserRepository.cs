﻿using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Security.Claims;

namespace GeekStreamProject.Infrastructure.DataAccess
{
    public class CurrentlyAuthenticatedUserRepository : ICurrentlyAuthenticatedUserRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CurrentlyAuthenticatedUserRepository(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public ApplicationUser GetCurrentlyAuthenticatedUser()
        {
            var userId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (userId == null)
            {
                return null;
            }

            return _context
                    .Users
                    .Include(user => user.Comments)
                    .Include(user => user.Subscribers)
                    .Include(user => user.Subscriptions)
                    .Include(user => user.Avatar)
                    .Include(user => user.Articles)
                        .ThenInclude(article => article.Category)
                    .FirstOrDefault(user => user.Id == userId);
        }
    }
}
