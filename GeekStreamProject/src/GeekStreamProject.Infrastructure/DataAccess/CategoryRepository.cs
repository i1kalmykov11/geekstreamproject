﻿using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace GeekStreamProject.Infrastructure.DataAccess
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly ApplicationDbContext _context;
        public CategoryRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<ArticleCategory> GetCategories()
        {
            return _context
                .Categories
                .Include(category => category.Image)
                .Include(category => category.Articles)
                .ToList();
        }

        public ArticleCategory GetCategory(int id)
        {
            return _context
                .Categories
                .Include(category => category.Image)
                .Include(category => category.Articles)
                .FirstOrDefault(category => category.Id == id);
        }

        public void Create(ArticleCategory category)
        {
            _context.Categories.Add(category);
            _context.SaveChanges();
        }
    }
}
