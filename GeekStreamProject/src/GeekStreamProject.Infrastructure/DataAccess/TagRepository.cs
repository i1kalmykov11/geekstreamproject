﻿using GeekStreamProject.Core.Shared;
using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace GeekStreamProject.Infrastructure.DataAccess
{
    public class TagRepository : ITagRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IArticleRepository _articleRepository;

        public TagRepository(ApplicationDbContext context, IArticleRepository articleRepository)
        {
            _context = context;
            _articleRepository = articleRepository;
        }

        public void CreateTagsFromStringInput(string[] line)
        {
            foreach(var tag in line)
            {
                if (!IsTagExist(tag))
                {
                    var newTag = new Tag
                    {
                        Content = tag,
                    };
                    CreateTag(newTag);
                }
            }
        }

        public void CreateTag(Tag tag)
        {
            _context.Tags.Add(tag);

            _context.SaveChanges();
        }

        public bool IsTagExist(string tagContent)
        {
            var tag = _context.Tags.FirstOrDefault(item => item.Content == tagContent);

            if (tag == null) return false;

            return true;
        }

        public Tag GetTag(string content)
        {
            var tag = _context.Tags.FirstOrDefault(item => item.Content == content);

            if (tag == null) return null;

            return tag;
        }

        public string TagToString(IEnumerable<string> tags)
        {
            string resultLine = "";
            foreach(var tag in tags)
            {
                resultLine += tag + " ";
            }
            return resultLine;
        }
    }
}
