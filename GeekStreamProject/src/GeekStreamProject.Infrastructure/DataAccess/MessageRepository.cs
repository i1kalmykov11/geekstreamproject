﻿using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace GeekStreamProject.Infrastructure.DataAccess
{
    public class MessageRepository : IMessageRepository
    {
        private readonly ApplicationDbContext _context;

        public MessageRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void CreateChat(Chat chat)
        {
            _context
                .Chats
                .Add(chat);

            _context
                .SaveChanges();
        }

        public Chat GetChat(int id)
        {
            return _context
                    .Chats
                    .Include(chat => chat.Messages)
                        .ThenInclude(message => message.Author)
                    .FirstOrDefault(chat => chat.Id == id);
        }


        public IEnumerable<ChatMessage> GetLatestChatMessages(int id)
        {
            return _context
                .ChatMessages
                .Include(message => message.Author)
                .OrderByDescending(message => message.SendedTime)
                .Take(50)
                .Where(message => message.ChatId == id)
                .ToList()
                .OrderBy(message => message.SendedTime);
        }

        public void CreateChatMessage(ChatMessage chatMessage)
        {
            _context
                .ChatMessages
                .Add(chatMessage);

            _context
                .SaveChanges();
        }

        public IEnumerable<DialogMessage> GetLatestDialogMessages(string user1Id, string user2Id)
        {
            return _context
                .Dialogs
                .Include(message => message.Author)
                .Include(message => message.Reciever)
                .Where(message => (message.AuthorId == user1Id & message.RecieverId == user2Id) 
                                | (message.AuthorId == user2Id & message.RecieverId == user1Id))
                .OrderByDescending(message => message.SendedTime)
                .Take(50)
                .ToList()
                .OrderBy(message => message.SendedTime);
        }

        public void CreateDialogMessage(DialogMessage dialogMessage)
        {
            _context
                .Dialogs
                .Add(dialogMessage);

            _context
                .SaveChanges();
        }

        public IEnumerable<Chat> GetChats()
        {
            return _context
                    .Chats
                    .Include(chat => chat.Messages)
                    .ToList();
        }
    }
}
