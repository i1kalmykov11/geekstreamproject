﻿using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace GeekStreamProject.Infrastructure.DataAccess
{
    public class SubscriptionRepository : ISubcriptionRepository
    {
        private readonly ApplicationDbContext _context;
        
        public SubscriptionRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<ApplicationUser> GetSubscribers(string userId)
        {
            return _context
                        .Subs
                        .Include(sub => sub.Subscriber)
                            .ThenInclude(sub => sub.Comments)
                         .Include(sub => sub.Subscriber)
                            .ThenInclude(sub => sub.Avatar)
                        .Include(sub => sub.Subscriber)
                            .ThenInclude(sub => sub.Articles)
                        .Where(sub => sub.SubscriptionId == userId)
                        .Select(sub => sub.Subscriber)
                        .ToList();
        }

        public IEnumerable<ApplicationUser> GetSubscriptions(string userId)
        {
            return _context
                        .Subs
                        .Include(sub => sub.Subscription)
                            .ThenInclude(sub => sub.Comments)
                         .Include(sub => sub.Subscription)
                            .ThenInclude(sub => sub.Avatar)
                        .Include(sub => sub.Subscription)
                            .ThenInclude(sub => sub.Articles)
                        .Where(sub => sub.SubscriberId == userId)
                        .Select(sub => sub.Subscription)
                        .ToList();
        }

        public void Subscribe(string subscriberId, string subscriptionId)
        {
            var sub = new SubscriptionSystem()
            {
                SubscriberId = subscriberId,
                SubscriptionId = subscriptionId,
            };

            _context
                .Subs
                .Add(sub);

            _context
                .SaveChanges();
        }

        public void Unsubscribe(string subscriberId, string subscriptionId)
        {
            var sub = _context
                        .Subs
                        .FirstOrDefault(subs => subs.SubscriptionId == subscriptionId & subs.SubscriberId == subscriberId);

            if (sub != null)
            {
                _context
                    .Subs
                    .Remove(sub);

                _context
                    .SaveChanges();
            }
        }

        public bool IsSubscribed(string subscriberId, string subscriptionId)
        {
            var sub = _context
                         .Subs
                         .FirstOrDefault(subs => subs.SubscriptionId == subscriptionId & subs.SubscriberId == subscriberId);

            return (sub != null) ? true : false;
        }
    }
}
