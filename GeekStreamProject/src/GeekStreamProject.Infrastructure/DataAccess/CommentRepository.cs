﻿using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStreamProject.Infrastructure.DataAccess
{
    public class CommentRepository : ICommentRepository
    {
        private readonly ApplicationDbContext _context;

        public CommentRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Create(Comment comment)
        {
            _context
                .Comments
                .Add(comment);

            _context
                .SaveChanges();
        }

        public void ChangeHonor(int id, int delta)
        {
            var comment = _context.Comments.FirstOrDefault(comment => comment.Id == id);

            comment.Honor += delta;

            _context
                .SaveChanges();
        }
    }
}
