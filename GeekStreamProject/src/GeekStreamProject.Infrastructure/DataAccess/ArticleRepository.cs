﻿using GeekStreamProject.Core.Shared;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GeekStreamProject.Infrastructure.DataAccess
{
    public class ArticleRepository : IArticleRepository
    {
        private ApplicationDbContext _context;

        public ArticleRepository(ApplicationDbContext context)
        {
            this._context = context;
        }

        public IEnumerable<Article> GetPublishedArticles()
        {
            return _context
                .Articles
                .Include(article => article.Category)
                .Include(article => article.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(article => article.Image)
                .Include(article => article.ArticleTags)
                    .ThenInclude(articleTag => articleTag.Tag)
                .Include(article => article.Comments)
                    .ThenInclude(comments => comments.Author)
                .Where(article => article.IsPublished == true)
                .OrderByDescending(comment => comment.PublishedDate)
                .Take(50)
                .ToList()
                .OrderBy(comment => comment.PublishedDate);
        }

        public IEnumerable<Article> GetLatestPublishedUserArticles(string userId)
        {
            return _context
                .Articles
                .Include(article => article.Category)
                .Include(article => article.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(article => article.Image)
                .Include(article => article.ArticleTags)
                    .ThenInclude(article => article.Tag)
                .Include(article => article.Comments)
                    .ThenInclude(comments => comments.Author)
                .Where(article => article.IsPublished == true & article.AuthorId == userId)
                .OrderByDescending(comment => comment.PublishedDate)
                .Take(50)
                .ToList()
                .OrderBy(comment => comment.PublishedDate);
        }

        public IEnumerable<Article> GetNotPublishedUserArticles(string id)
        {
            return _context
                .Articles
                .Include(article => article.Category)
                .Include(article => article.Image)
                .Include(article => article.ArticleTags)
                    .ThenInclude(article => article.Tag)
                .Include(article => article.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(article => article.Comments)
                    .ThenInclude(comments => comments.Author)
                .Where(article => article.AuthorId == id & article.IsPublished == false)
                .OrderByDescending(comment => comment.CreatedDate)
                .Take(50)
                .ToList()
                .OrderBy(comment => comment.CreatedDate);
        }

        public IEnumerable<Article> GetNotPublishedArticles()
        {
            return _context
                .Articles
                .Include(article => article.Category)
                .Include(article => article.Image)
                .Include(article => article.ArticleTags)
                    .ThenInclude(article => article.Tag)
                .Include(article => article.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(article => article.Comments)
                    .ThenInclude(comments => comments.Author)
                .OrderByDescending(comment => comment.CreatedDate)
                .Take(50)
                .ToList()
                .OrderBy(comment => comment.CreatedDate);
        }

        public IEnumerable<Article> GetNotApprovedArticles()
        {
            return _context
                .Articles
                .Include(article => article.Category)
                .Include(article => article.Image)
                .Include(article => article.ArticleTags)
                    .ThenInclude(article => article.Tag)
                .Include(article => article.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(article => article.Comments)
                    .ThenInclude(comments => comments.Author)
                .Where(article => article.IsApproved == false)
                .OrderByDescending(comment => comment.CreatedDate)
                .Take(50)
                .ToList()
                .OrderBy(comment => comment.CreatedDate);
        }

        public IEnumerable<Article> GetApprovedArticles()
        {
            return _context
                .Articles
                .Include(article => article.Category)
                .Include(article => article.Image)
                .Include(article => article.ArticleTags)
                    .ThenInclude(article => article.Tag)
                .Include(article => article.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(article => article.Comments)
                    .ThenInclude(comments => comments.Author)
                .Where(article => article.IsApproved == true)
                .OrderByDescending(comment => comment.ApprovedDate)
                .Take(50)
                .ToList()
                .OrderBy(comment => comment.ApprovedDate);
        }

        public IEnumerable<Article> GetPublishedArticlesByCategory(int categoryId)
        {
            return _context
                .Articles
                .Where(article => article.CategoryId == categoryId && article.IsPublished)
                .Include(article => article.Category)
                .Include(article => article.ArticleTags)
                    .ThenInclude(article => article.Tag)
                .Include(article => article.Image)
                .Include(article => article.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(article => article.Comments)
                    .ThenInclude(comments => comments.Author)
                .OrderByDescending(comment => comment.PublishedDate)
                .Take(50)
                .ToList()
                .OrderBy(comment => comment.PublishedDate);
        }

        public Article GetArticleInfo(int id)
        {
            return _context
                .Articles
                .Include(article => article.Category)
                .Include(article => article.Image)
                .Include(article => article.ArticleTags)
                    .ThenInclude(article => article.Tag)
                .Include(article => article.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(article => article.Comments)
                    .ThenInclude(comments => comments.Author)
                .FirstOrDefault(article => article.Id == id);
        }

        public void ApproveArticle(int id)
        {
            var article = _context.Articles.FirstOrDefault(article => article.Id == id);

            if (article != null)
            {
                article.IsApproved = true;
                article.ApprovedDate = DateTime.UtcNow;
                _context
                    .SaveChanges();
            }    
        }

        public void PublishArticle(int id)
        {
            var article = _context.Articles.FirstOrDefault(article => article.Id == id);

            if(article != null && article.IsApproved)
            {
                article.IsPublished = true;
                article.PublishedDate = DateTime.UtcNow;
                _context
                    .SaveChanges();
            }   
        }

        public void DisApproveArticle(int id)
        {
            var article = _context.Articles.FirstOrDefault(article => article.Id == id);

            if (article != null)
            {
                if (article.IsApproved)
                {
                    if (article.IsPublished)
                    {
                        article.IsPublished = false;
                        article.PublishedDate = null;
                    }
                    article.IsApproved = false;
                    article.ApprovedDate = null;
                }

                _context
                    .SaveChanges();
            }
        }

        public void DisPublishArticle(int id)
        {
            var article = _context.Articles.FirstOrDefault(article => article.Id == id);

            if (article != null)
            {
                if (article.IsPublished)
                {
                    article.IsPublished = false;
                    article.PublishedDate = null;
                }

                _context
                    .SaveChanges();
            }
        }

        public void Create(Article article)
        {
            _context
                .Articles
                .Add(article);
            _context.SaveChanges();
        }

        public void Edit(Article changedArticle)
        {
            var article = _context.Articles.FirstOrDefault(article => article.Id == changedArticle.Id);

            if ( article != null )
            {
                if(article != changedArticle)
                {
                    _context.Articles.Remove(article);
                    _context.Articles.Add(changedArticle);
                }
                _context.
                        SaveChanges();
            }
        }

        public void Delete(int id)
        {
            var article = _context.Articles.FirstOrDefault(article => article.Id == id);

            if (article != null)
            {
                _context
                    .Articles
                    .Remove(article);
                _context
                    .SaveChanges();
            }
        }

        public void ChangeHonor(int id, int delta)
        {
            var article = _context.Articles.FirstOrDefault(article => article.Id == id);

            article.Honor += delta;

            _context
                .SaveChanges();
        }
    }
}
