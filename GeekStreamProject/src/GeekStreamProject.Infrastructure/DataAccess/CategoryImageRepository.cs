﻿using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GeekStreamProject.Infrastructure.DataAccess
{
    public class CategoryImageRepository : ICategoryImageRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IFileRepository _fileRepository;

        public CategoryImageRepository(ApplicationDbContext context, IFileRepository fileRepository)
        {
            _context = context;
            _fileRepository = fileRepository;
        }

        public void Create(CategoryImage categoryImage)
        {
            _context
                .CategoryImages
                .Add(categoryImage);

            _context
                .SaveChanges();
        }

        public string SaveImage(IFormFile avatar, IFormFile backGround, string directoryName)
        {
            var imageId = _fileRepository.SaveFile(avatar, directoryName);
            var backGroundId = _fileRepository.SaveFile(backGround, directoryName);

            var categoryImage = new CategoryImage()
            {
                Id = imageId,
                FileName = avatar.FileName,
                Path = Path.Combine("Assets/Imgs/" + directoryName + "/", imageId + Path.GetExtension(avatar.FileName)),
                BackGroundFileName = backGround.FileName,
                BackGroundPath = Path.Combine("Assets/Imgs/" + directoryName + "/", backGroundId + Path.GetExtension(backGround.FileName)),
            };

            Create(categoryImage);

            return categoryImage.Id;

        }


        public void Delete(string id)
        {
            var image = _context
                            .CategoryImages
                            .FirstOrDefault(image => image.Id == id);

            if (image != null)
            {
                _fileRepository.DeleteFile(image.Path);
                _fileRepository.DeleteFile(image.BackGroundPath);

                _context
                    .CategoryImages
                    .Remove(image);

                _context.SaveChanges();
            }
        }


        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
