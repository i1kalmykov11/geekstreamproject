﻿using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace GeekStreamProject.Infrastructure.DataAccess
{
    public class ApplicationUserRepository : IUserRepository
    {
        private readonly ApplicationDbContext _context;

        public ApplicationUserRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public ApplicationUser GetUserById(string id)
        {
            return _context
                .Users
                .Include(user => user.Comments)
                .Include(user => user.Subscribers)
                .Include(user => user.Subscriptions)
                .Include(user => user.Avatar)
                .Include(user => user.Articles)
                    .ThenInclude(article => article.Category)
                .FirstOrDefault(user => user.Id == id);
        }

        public IEnumerable<ApplicationUser> GetUsers()
        {
            return _context
                .Users
                .Include(user => user.Comments)
                .Include(user => user.Subscribers)
                .Include(user => user.Avatar)
                .Include(user => user.Subscriptions)
                .Include(user => user.Articles)
                    .ThenInclude(article => article.Category)
                .ToList();
        }

        public IEnumerable<ApplicationUser> GetAuthors()
        {
            var authors = _context
                .Users
                .Include(user => user.Comments)
                .Include(user => user.Subscribers)
                .Include(user => user.Avatar)
                .Include(user => user.Subscriptions)
                .Include(user => user.Articles)
                    .ThenInclude(article => article.Category)
                .Where(user => user.Articles.Any())
                .ToList();
            return authors;
        }

        public void Edit(ApplicationUser changedUser)
        {
            var user = _context.Users.FirstOrDefault(user => user.Id == changedUser.Id);

            if(user != null)
            {
                if (user.FirstName != changedUser.FirstName)
                {
                    user.FirstName = changedUser.FirstName;
                }
                if(user.LastName != changedUser.LastName)
                {
                    user.LastName = changedUser.LastName;
                }
                if(user.Email != changedUser.Email)
                {
                    user.Email = changedUser.Email;
                }
                if(user.AvatarId != changedUser.AvatarId)
                {
                    user.AvatarId = changedUser.AvatarId;
                }
                
                _context
                    .SaveChanges();
            }
        }
    }
}
