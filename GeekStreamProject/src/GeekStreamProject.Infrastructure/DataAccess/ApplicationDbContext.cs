﻿using GeekStreamProject.Core.Shared.Entities;
using GeekStreamProject.Infrastructure.Mapping;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace GeekStreamProject.Infrastructure.DataAccess
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string,
        IdentityUserClaim<string>, ApplicationUserRole, IdentityUserLogin<string>,IdentityRoleClaim<string>,
        IdentityUserToken<string>>
    {
        public DbSet<Article> Articles { get; set; }
        
        public DbSet<ArticleCategory> Categories { get; set; }
        
        public DbSet<Avatar> Avatars { get; set; }
        
        public DbSet<ArticleImage> ArticleImages { get; set; }

        public DbSet<CategoryImage> CategoryImages { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<SubscriptionSystem> Subs { get; set; }

        public DbSet<Chat> Chats { get; set; }

        public DbSet<ChatMessage> ChatMessages{ get; set; }

        public DbSet<DialogMessage> Dialogs { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<ArticleTag> ArticleTags { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            modelBuilder
                .Entity<ApplicationRole>()
                .HasData(new ApplicationRole { Id = "52C96CE9 - 7C1A - 4CFC - 9C73 - 68583C31670B", Name = "User", NormalizedName = "USER" }
                         , new ApplicationRole { Id = "787D978D - 5DD3 - 436F - 847C - 130E9F755C38", Name = "Moder", NormalizedName = "MODER" }
                        );
            modelBuilder
                .Entity<Avatar>()
                .HasData(new Avatar { Id = "5FCE67C0-5434-4970-A9F0-CD94F58C70DA", Path = "Assets/Imgs/Logo.png", FileName = "Logo.png" }
                        );

            modelBuilder.ApplyConfiguration(new SubscriptionSystemMap());
            modelBuilder.ApplyConfiguration(new ApplicationRoleMap());
            modelBuilder.ApplyConfiguration(new ApplicationUserRoleMap());
            modelBuilder.ApplyConfiguration(new ApplicationUserMap());
            modelBuilder.ApplyConfiguration(new ArticleCategoryMap());
            modelBuilder.ApplyConfiguration(new ArticleCategoryImageMap());
            modelBuilder.ApplyConfiguration(new ArticleMap());
            modelBuilder.ApplyConfiguration(new UserAvatarMap());
            modelBuilder.ApplyConfiguration(new ChatMap());
            modelBuilder.ApplyConfiguration(new ChatMessageMap());
            modelBuilder.ApplyConfiguration(new ArticleTagMap());
        }
    }
}
