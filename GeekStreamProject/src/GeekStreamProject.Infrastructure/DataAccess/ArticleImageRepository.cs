﻿using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;

namespace GeekStreamProject.Infrastructure.DataAccess
{
    public class ArticleImageRepository : IImageRepository<ArticleImage>
    {
        private readonly ApplicationDbContext _context;
        private readonly IFileRepository _fileRepository;

        public ArticleImageRepository(ApplicationDbContext context, IFileRepository fileRepository)
        {
            _context = context;
            _fileRepository = fileRepository;
        }

        public void Create(ArticleImage articleImage)
        {
            _context
                .ArticleImages
                .Add(articleImage);

            _context
                .SaveChanges();
        }

        public string SaveImage(IFormFile image, string directoryName)
        {
            var imageId = _fileRepository.SaveFile(image, directoryName);

            var articleImage = new ArticleImage()
            {
                Id = imageId,
                FileName = image.FileName,
                Path = Path.Combine("Assets/Imgs/" + directoryName + "/", imageId + Path.GetExtension(image.FileName)),
            };

            Create(articleImage);

            return articleImage.Id;

        }

        public void Delete(string id)
        {
            var image = _context.ArticleImages.FirstOrDefault(image => image.Id == id);

            if (image != null)
            {
                _fileRepository.DeleteFile(image.Path);

                _context.ArticleImages.Remove(image);

                _context.SaveChanges();
            }
        }


        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
