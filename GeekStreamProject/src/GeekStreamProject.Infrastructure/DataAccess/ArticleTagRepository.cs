﻿using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace GeekStreamProject.Infrastructure.DataAccess
{
    public class ArticleTagRepository : IArticleTagRepository
    {

        private readonly ApplicationDbContext _context;

        public ArticleTagRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public ArticleTag BuildEntity(int articleId, int tagId)
        {
            return new ArticleTag
            {
                ArticleId = articleId,
                TagId = tagId,
            };
        }

        public void Create(ArticleTag entity)
        {
            _context
                .ArticleTags
                .Add(entity);

            _context.SaveChanges();
        }

       public IEnumerable<Article> GetTaggedArticle(int tagId)
       {
            return _context
                        .ArticleTags
                        .Where(tag => tag.TagId == tagId)
                        .Include(article => article.Article)
                            .ThenInclude(article => article.Category)
                        .Include(article => article.Article)
                            .ThenInclude(article => article.Author)
                            .ThenInclude(author => author.Avatar)
                        .Include(article => article.Article)
                            .ThenInclude(article => article.Image)
                        .Include(article => article.Article)
                        .ThenInclude(article => article.Comments)
                            .ThenInclude(comments => comments.Author)
                        .Include(article => article.Article)
                            .ThenInclude(article => article.ArticleTags).ThenInclude(tag => tag.Tag)
                        .Select(tag => tag.Article).Where(article => article.IsPublished == true)
                        .ToList();
       }
    }
}
