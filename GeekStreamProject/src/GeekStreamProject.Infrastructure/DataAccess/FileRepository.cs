﻿using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using SixLabors.ImageSharp;
using System;
using System.IO;

namespace GeekStreamProject.Infrastructure.DataAccess
{
    public class FileRepository : IFileRepository
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public FileRepository(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public string SaveFile(IFormFile image, string directoryName)
        {
            const string assetEirarchy = "Assets/Imgs/";
            string uploadsFolder = Path.Combine(_hostingEnvironment.WebRootPath, assetEirarchy + directoryName);
            var imageId = Guid.NewGuid().ToString();
            string uniqueFileName = imageId + Path.GetExtension(image.FileName);
            string filePath = Path.Combine(uploadsFolder, uniqueFileName);

            using var imageLoad = Image.Load(image.OpenReadStream());

            imageLoad.Save(filePath);

            return imageId;
        }

        public void DeleteFile(string path)
        {
            if (path != null)
            {
                File.Delete(Path.Combine(_hostingEnvironment.WebRootPath, path));
            }
        }
    }
}
