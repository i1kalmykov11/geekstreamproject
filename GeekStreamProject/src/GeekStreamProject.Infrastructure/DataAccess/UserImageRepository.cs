﻿using GeekStreamProject.Core.Shared.Abstractions.DataAccess;
using GeekStreamProject.Core.Shared.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;

namespace GeekStreamProject.Infrastructure.DataAccess
{
    public class UserImageRepository : IImageRepository<Avatar>
    {
        private readonly ApplicationDbContext _context;
        private readonly IFileRepository _fileRepository;

        public UserImageRepository(ApplicationDbContext context, IFileRepository fileRepository)
        {
            _context = context;
            _fileRepository = fileRepository;
        }

        public void Create(Avatar avatar)
        {
            _context
                .Avatars
                .Add(avatar);

            _context
                .SaveChanges();
        }

        public string SaveImage(IFormFile image, string directoryName)
        {
            var imageId = _fileRepository.SaveFile(image, directoryName);

            var avatar = new Avatar()
            {
                Id = imageId,
                FileName = image.FileName,
                Path = Path.Combine("Assets/Imgs/" + directoryName + "/", imageId + Path.GetExtension(image.FileName)),
            };

            Create(avatar);

            return avatar.Id;

        }

        public void Delete(string id)
        {
            var image = _context
                            .Avatars
                            .FirstOrDefault(image => image.Id == id);

            if (image != null)
            {
                _fileRepository.DeleteFile(image.Path);

                _context
                    .Avatars
                    .Remove(image);

                _context.SaveChanges();
            }
        }


        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
