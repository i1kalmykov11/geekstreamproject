﻿using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GeekStreamProject.Infrastructure.Mapping
{
    public class SubscriptionSystemMap : IEntityTypeConfiguration<SubscriptionSystem>
    {
        public void Configure(EntityTypeBuilder<SubscriptionSystem> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Subscription)
                   .WithMany(x => x.Subscribers)
                   .OnDelete(DeleteBehavior.Restrict)
                   .HasForeignKey(x => x.SubscriptionId);

            builder.HasOne(x => x.Subscriber)
                   .WithMany(x => x.Subscriptions)
                   .OnDelete(DeleteBehavior.Restrict)
                   .HasForeignKey(x => x.SubscriberId);
        }
    }
}
