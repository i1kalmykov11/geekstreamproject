﻿using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GeekStreamProject.Infrastructure.Mapping
{
    public class ArticleMap : IEntityTypeConfiguration<Article>
    {
        public void Configure(EntityTypeBuilder<Article> builder)
        {
            builder.Property(x => x.Title)
                .HasColumnType("nvarchar(300)")
                .IsRequired();

            builder.Property(x => x.Content)
                .HasColumnType("ntext")
                .IsRequired();

            builder.HasOne(x => x.Author)
                .WithMany(x => x.Articles)
                .HasForeignKey(x => x.AuthorId);

            builder.HasOne(x => x.Category)
                .WithMany(x => x.Articles)
                .HasForeignKey(x => x.CategoryId);

            builder.HasOne(x => x.Image)
                .WithOne(x => x.Article)
                .HasForeignKey<Article>(x => x.ImageId);

        }
    }
}
