﻿using GeekStreamProject.Core.Shared.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GeekStreamProject.Infrastructure.Mapping
{
    public class ApplicationUserMap : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            builder.Property(x => x.FirstName)
                .HasColumnType("nvarchar(150)")
                .IsRequired();
            
            builder.Property(x => x.LastName)
                .HasColumnType("nvarchar(150)")
                .IsRequired();

            builder.Property(x => x.UserName)
                .HasColumnType("nvarchar(150)")
                .IsRequired();

            builder.HasOne(x => x.Avatar)
                .WithMany(x => x.Users)
                .HasForeignKey(x => x.AvatarId);

            builder.HasMany(x => x.ApplicationUserRoles)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId);

            builder.HasMany(x => x.Articles)
                .WithOne(x => x.Author)
                .HasForeignKey(x => x.AuthorId);

            builder.HasMany(x => x.Comments)
                .WithOne(x => x.Author)
                .HasForeignKey(x => x.AuthorId);

        }
    }
}
